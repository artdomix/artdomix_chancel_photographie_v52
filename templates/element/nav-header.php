<div class="nav-header">
    <a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'index'] ); ?>" class="brand-logo">
        <img class="logo-abbr" src="<?= $this->Url->webroot('/'); ?>images/logo.png" alt="/">
        <img class="logo-compact" src="<?= $this->Url->webroot('/'); ?>images/logo-text.png" alt="/">
        <img class="brand-title" src="<?= $this->Url->webroot('/'); ?>images/logo-text.png" alt="/">
    </a>

    <div class="nav-control">
        <div class="hamburger">
            <span class="line"></span><span class="line"></span><span class="line"></span>
        </div>
    </div>
</div>