<div class="deznav">
    <div class="deznav-scroll dz-scroll">
        <ul class="metismenu" id="menu">
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-networking"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Dashboard'), array('controller' => 'SegoAdmin', 'action' => 'index')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Orders'), array('controller' => 'SegoAdmin', 'action' => 'orders')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Order ID'), array('controller' => 'SegoAdmin', 'action' => 'order_id')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('General Customers'), array('controller' => 'SegoAdmin', 'action' => 'general_customers')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Analytics'), array('controller' => 'SegoAdmin', 'action' => 'analytics')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Reviews'), array('controller' => 'SegoAdmin', 'action' => 'reviews')); ?>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-television"></i>
                    <span class="nav-text">Apps</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Profile'), array('controller' => 'SegoAdmin', 'action' => 'app_profile')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Post Details'), array('controller' => 'SegoAdmin', 'action' => 'post_details')); ?>
                    </li>
                    <li><a class="has-arrow" href="javascript:void(0);" aria-expanded="false">Email</a>
                        <ul aria-expanded="false">
                            <li>
                                <?= $this->HTML->link(__('Compose'), array('controller' => 'SegoAdmin', 'action' => 'email_compose')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Inbox'), array('controller' => 'SegoAdmin', 'action' => 'email_inbox')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Read'), array('controller' => 'SegoAdmin', 'action' => 'email_read')); ?>
                            </li>
                        </ul>
                    </li>
                    <li><a
                            href="<?= $this->Url->build(['controller' => 'SegoAdmin', 'action' => 'app_calender']); ?>">Calendar</a>
                    </li>
                    <li><a class="has-arrow" href="javascript:void(0);" aria-expanded="false">Shop</a>
                        <ul aria-expanded="false">
                            <li>
                                <?= $this->HTML->link(__('Product Grid'), array('controller' => 'SegoAdmin', 'action' => 'ecom_product_grid')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Product List'), array('controller' => 'SegoAdmin', 'action' => 'ecom_product_list')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Product Details'), array('controller' => 'SegoAdmin', 'action' => 'ecom_product_detail')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Order'), array('controller' => 'SegoAdmin', 'action' => 'ecom_product_order')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Checkout'), array('controller' => 'SegoAdmin', 'action' => 'ecom_checkout')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Invoice'), array('controller' => 'SegoAdmin', 'action' => 'ecom_invoice')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Customers'), array('controller' => 'SegoAdmin', 'action' => 'ecom_customers')); ?>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-file"></i>
                    <span class="nav-text">Icons</span>

                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Flaticons'), array('controller' => 'SegoAdmin', 'action' => 'flat_icons')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('SVG Icons'), array('controller' => 'SegoAdmin', 'action' => 'svg_icons')); ?>
                    </li>

                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-database-1"></i>
                    <span class="nav-text">CMS</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Content'), array('controller' => 'SegoAdmin', 'action' => 'content')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Add Content'), array('controller' => 'SegoAdmin', 'action' => 'content_add')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Menus'), array('controller' => 'SegoAdmin', 'action' => 'menu')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Email Template'), array('controller' => 'SegoAdmin', 'action' => 'email_template')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Add Email'), array('controller' => 'SegoAdmin', 'action' => 'add_email')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Blog'), array('controller' => 'SegoAdmin', 'action' => 'blog')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Add Blog'), array('controller' => 'SegoAdmin', 'action' => 'add_blog')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Blog Category'), array('controller' => 'SegoAdmin', 'action' => 'blog_category')); ?>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-controls-3"></i>
                    <span class="nav-text">Charts</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Flot'), array('controller' => 'SegoAdmin', 'action' => 'chart_flot')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Morris'), array('controller' => 'SegoAdmin', 'action' => 'chart_morris')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Chartjs'), array('controller' => 'SegoAdmin', 'action' => 'chart_chartjs')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Chartist'), array('controller' => 'SegoAdmin', 'action' => 'chart_chartist')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Sparkline'), array('controller' => 'SegoAdmin', 'action' => 'chart_sparkline')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Peity'), array('controller' => 'SegoAdmin', 'action' => 'chart_peity')); ?>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-internet"></i>
                    <span class="nav-text">Bootstrap</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Accordion'), array('controller' => 'SegoAdmin', 'action' => 'ui_accordion')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Alert'), array('controller' => 'SegoAdmin', 'action' => 'ui_alert')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Badge'), array('controller' => 'SegoAdmin', 'action' => 'ui_badge')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Button'), array('controller' => 'SegoAdmin', 'action' => 'ui_button')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Modal'), array('controller' => 'SegoAdmin', 'action' => 'ui_modal')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Button Group'), array('controller' => 'SegoAdmin', 'action' => 'ui_button-group')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('List Group'), array('controller' => 'SegoAdmin', 'action' => 'ui_list_group')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Media Object'), array('controller' => 'SegoAdmin', 'action' => 'ui_media_object')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Cards'), array('controller' => 'SegoAdmin', 'action' => 'ui_card')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Carousel'), array('controller' => 'SegoAdmin', 'action' => 'ui_carousel')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Dropdown'), array('controller' => 'SegoAdmin', 'action' => 'ui_dropdown')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Popover'), array('controller' => 'SegoAdmin', 'action' => 'ui_popover')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Progressbar'), array('controller' => 'SegoAdmin', 'action' => 'ui_progressbar')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Tab'), array('controller' => 'SegoAdmin', 'action' => 'ui_tab')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Typography'), array('controller' => 'SegoAdmin', 'action' => 'ui_typography')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Pagination'), array('controller' => 'SegoAdmin', 'action' => 'ui_pagination')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Grid'), array('controller' => 'SegoAdmin', 'action' => 'ui_grid')); ?>
                    </li>

                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-heart"></i>
                    <span class="nav-text">Plugins</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Select 2'), array('controller' => 'SegoAdmin', 'action' => 'uc_select2')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Nestedable'), array('controller' => 'SegoAdmin', 'action' => 'uc_nestable')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Noui Slider'), array('controller' => 'SegoAdmin', 'action' => 'uc_noui-slider')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Sweet Alert'), array('controller' => 'SegoAdmin', 'action' => 'uc_sweetalert')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Toastr'), array('controller' => 'SegoAdmin', 'action' => 'uc_toastr')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Jqv Map'), array('controller' => 'SegoAdmin', 'action' => 'map_jqvmap')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Light Gallery'), array('controller' => 'SegoAdmin', 'action' => 'uc_lightgallery')); ?>
                    </li>
                </ul>
            </li>
            <li><a href="<?= $this->Url->build(['controller' => 'SegoAdmin', 'action' => 'widget_basic']); ?>"
                    class="ai-icon" aria-expanded="false">
                    <i class="flaticon-381-settings-2"></i>
                    <span class="nav-text">Widget</span>
                </a>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-notepad"></i>
                    <span class="nav-text">Forms</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Form Elements'), array('controller' => 'SegoAdmin', 'action' => 'form_element')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Wizard'), array('controller' => 'SegoAdmin', 'action' => 'form_wizard')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Form Editor'), array('controller' => 'SegoAdmin', 'action' => 'form_editor')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Pickers'), array('controller' => 'SegoAdmin', 'action' => 'form_pickers')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Jquery Validate'), array('controller' => 'SegoAdmin', 'action' => 'form_validation_jquery')); ?>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-network"></i>
                    <span class="nav-text">Table</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Bootstrap'), array('controller' => 'SegoAdmin', 'action' => 'table_bootstrap_basic')); ?>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Datatable'), array('controller' => 'SegoAdmin', 'action' => 'table_datatable_basic')); ?>
                    </li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void(0);" aria-expanded="false">
                    <i class="flaticon-381-layer-1"></i>
                    <span class="nav-text">Pages</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?= $this->HTML->link(__('Register'), array('controller' => 'SegoAdmin', 'action' => 'page_register')); ?>
                    <li>
                        <?= $this->HTML->link(__('Login'), array('controller' => 'SegoAdmin', 'action' => 'page_login')); ?>
                    </li>
                    <li><a class="has-arrow" href="javascript:void(0);" aria-expanded="false">Error</a>
                        <ul aria-expanded="false">
                            <li>
                                <?= $this->HTML->link(__('Error 400'), array('controller' => 'SegoAdmin', 'action' => 'page_error_400')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Error 403'), array('controller' => 'SegoAdmin', 'action' => 'page_error_403')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Error 404'), array('controller' => 'SegoAdmin', 'action' => 'page_error_404')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Error 500'), array('controller' => 'SegoAdmin', 'action' => 'page_error_500')); ?>
                            </li>
                            <li>
                                <?= $this->HTML->link(__('Error 503'), array('controller' => 'SegoAdmin', 'action' => 'page_error_503')); ?>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <?= $this->HTML->link(__('Lock Screen'), array('controller' => 'SegoAdmin', 'action' => 'page_lock_screen')); ?>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="add-menu-sidebar">
            <img src="<?= $this->Url->webroot('/'); ?>images/food-serving.png" alt="/">
            <p class="mb-3">Organize your menus through button bellow</p>
            <span class="fs-12 d-block mb-3">Lorem ipsum dolor sit amet</span>
            <a href="javascript:void(0)" class="btn btn-secondary btn-rounded" data-bs-toggle="modal"
                data-bs-target="#addOrderModalside">+Add Menus</a>
        </div>
        <div class="copyright">
            <p><strong>Sego Restaurant Admin Dashboard</strong> © 2024 All Rights Reserved</p>
            <p>Made with <span class="heart"></span> by DexignZone</p>
        </div>
    </div>
</div>

<div class="modal fade" id="addOrderModalside">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Menus</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group mb-3">
                        <label class="form-label">Food Name</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">Order Date</label>
                        <input class="form-control" type="text" id="datepicker">
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">Food Price</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>