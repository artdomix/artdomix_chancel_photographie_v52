<div class="container-fluid">
	<!-- Add Order -->
	
	<div class="page-titles">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Form</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Editor</a></li>
		</ol>
	</div>
	<!-- row -->
	<div class="row">
		<div class="col-xl-12 col-xxl-12">
			<div class="card h-auto">
				<div class="card-header">
					<h4 class="card-title">CkEditor</h4>
				</div>
				<div class="card-body custom-ekeditor">
					<div id="ckeditor"></div>
				</div>
			</div>
			<div class="card h-auto">
				<div class="card-header">
					<h4 class="card-title">Summernote Editor</h4>
				</div>
				<div class="card-body">
					<div class="summernote"></div>
				</div>
			</div>
		</div>
	</div>
</div>