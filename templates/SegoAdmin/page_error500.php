<div class="col-md-6">
    <div class="form-input-content text-center error-page">
        <h1 class="error-text font-w700">500</h1>
        <h4><i class="fa fa-times-circle text-danger"></i> Internal Server Error</h4>
        <p>You do not have permission to view this resource</p>
        <div>
            <a class="btn btn-primary" href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'index'] ); ?>">Back to Home</a>
        </div>
    </div>
</div>