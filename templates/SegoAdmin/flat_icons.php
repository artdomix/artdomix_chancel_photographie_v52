<div class="container-fluid">
	<!-- Add Order -->
	
	<div class="page-titles">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Icons</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Flaticon Icons</a></li>
		</ol>
	</div>
	<!-- row -->
	<div class="card">
		<div class="card-body svg-area pb-1">
			<div class="row">
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg">
						<div class="svg-icons-prev">
							<i class="flaticon-381-add"></i>
						</div>
						<div class="svg-classname">flaticon-381-add</div>

						<div class="modal fade" id="svg_img_Brassieresvg" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg">flaticon-381-add</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-add"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-1">
						<div class="svg-icons-prev">
							<i class="flaticon-381-add-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-add-1</div>
						<div class="modal fade" id="svg_img_Brassieresvg-1" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-1">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-1">flaticon-381-add-1
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-add-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-2">
						<div class="svg-icons-prev">
							<i class="flaticon-381-add-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-add-2</div>
						<div class="modal fade" id="svg_img_Brassieresvg-2" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-2">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-2">flaticon-381-add-2
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-add-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-23">
						<div class="svg-icons-prev">
							<i class="flaticon-381-add-3"></i>
						</div>
						<div class="svg-classname">flaticon-381-add-3</div>
						<div class="modal fade" id="svg_img_Brassieresvg-23" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-23">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-23">flaticon-381-add-3
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-add-3"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-24">
						<div class="svg-icons-prev">
							<i class="flaticon-381-alarm-clock"></i>
						</div>
						<div class="svg-classname">flaticon-381-alarm-clock</div>
						<div class="modal fade" id="svg_img_Brassieresvg-24" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-24">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-24">
											flaticon-381-alarm-clock</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-alarm-clock"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-25">
						<div class="svg-icons-prev">
							<i class="flaticon-381-alarm-clock-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-alarm-clock-1</div>
						<div class="modal fade" id="svg_img_Brassieresvg-25" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-25">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-25">
											flaticon-381-alarm-clock-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-alarm-clock-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-26">
						<div class="svg-icons-prev"><i class="flaticon-381-album"></i></div>
						<div class="svg-classname">flaticon-381-album</div>

						<div class="modal fade" id="svg_img_Brassieresvg-26" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-26">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-26">flaticon-381-album
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-album"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-27">
						<div class="svg-icons-prev">
							<i class="flaticon-381-album-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-album-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-27" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-27">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-27">flaticon-381-album-1
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-album-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-28">
						<div class="svg-icons-prev">
							<i class="flaticon-381-album-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-album-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-28" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-28">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-28">flaticon-381-album-2
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-album-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-29">
						<div class="svg-icons-prev">
							<i class="flaticon-381-album-3"></i>
						</div>
						<div class="svg-classname">flaticon-381-album-3</div>
						<div class="modal fade" id="svg_img_Brassieresvg-29" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-29">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-29">flaticon-381-album-3
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-album-3"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-30">
						<div class="svg-icons-prev">
							<i class="flaticon-381-app"></i>
						</div>
						<div class="svg-classname">flaticon-381-app</div>

						<div class="modal fade" id="svg_img_Brassieresvg-30" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-30">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-30">flaticon-381-app</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-app"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-31">
						<div class="svg-icons-prev">
							<i class="flaticon-381-archive"></i>
						</div>
						<div class="svg-classname">flaticon-381-archive</div>

						<div class="modal fade" id="svg_img_Brassieresvg-31" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-31">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-31">flaticon-381-archive
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-archive"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-32">
						<div class="svg-icons-prev">
							<i class="flaticon-381-back"></i>
						</div>
						<div class="svg-classname">flaticon-381-back</div>

						<div class="modal fade" id="svg_img_Brassieresvg-32" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-32">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-32">flaticon-381-back
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-back"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-33">
						<div class="svg-icons-prev">
							<i class="flaticon-381-back-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-back-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-33" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-33">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-33">flaticon-381-back-1
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-back-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-34">
						<div class="svg-icons-prev">
							<i class="flaticon-381-back-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-back-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-34" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-34">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-34">flaticon-381-back-2
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-back-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-35">
						<div class="svg-icons-prev">
							<i class="flaticon-381-background"></i>
						</div>
						<div class="svg-classname">flaticon-381-background</div>

						<div class="modal fade" id="svg_img_Brassieresvg-35" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-35">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-35">
											flaticon-381-background</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-background"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-36">
						<div class="svg-icons-prev">
							<i class="flaticon-381-background-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-background-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-36" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-36">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-36">
											flaticon-381-background-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-background-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-37">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery</div>

						<div class="modal fade" id="svg_img_Brassieresvg-37" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-37">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-37">flaticon-381-battery
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-38">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-38" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-38">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-38">
											flaticon-381-battery-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-39">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-39" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-39">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-39">
											flaticon-381-battery-2</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-40">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-3"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-3</div>

						<div class="modal fade" id="svg_img_Brassieresvg-40" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-40">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-40">
											flaticon-381-battery-3</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-3"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-41">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-4"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-4</div>

						<div class="modal fade" id="svg_img_Brassieresvg-41" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-41">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-41">
											flaticon-381-battery-4</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-4"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-42">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-5"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-5</div>

						<div class="modal fade" id="svg_img_Brassieresvg-42" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-42">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-42">
											flaticon-381-battery-5</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-5"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-43">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-6"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-6</div>

						<div class="modal fade" id="svg_img_Brassieresvg-43" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-43">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-43">
											flaticon-381-battery-6</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-6"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-44">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-7"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-7</div>

						<div class="modal fade" id="svg_img_Brassieresvg-44" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-44">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-44">
											flaticon-381-battery-7</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-7"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-45">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-8"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-8</div>

						<div class="modal fade" id="svg_img_Brassieresvg-45" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-45">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-45">
											flaticon-381-battery-8</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-8"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-46">
						<div class="svg-icons-prev">
							<i class="flaticon-381-battery-9"></i>
						</div>
						<div class="svg-classname">flaticon-381-battery-9</div>

						<div class="modal fade" id="svg_img_Brassieresvg-46" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-46">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-46">
											flaticon-381-battery-9</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-battery-9"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-47">
						<div class="svg-icons-prev">
							<i class="flaticon-381-binoculars"></i>
						</div>
						<div class="svg-classname">flaticon-381-binoculars</div>

						<div class="modal fade" id="svg_img_Brassieresvg-47" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-47">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-47">
											flaticon-381-binoculars</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-binoculars"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-48">
						<div class="svg-icons-prev">
							<i class="flaticon-381-blueprint"></i>
						</div>
						<div class="svg-classname">flaticon-381-blueprint</div>

						<div class="modal fade" id="svg_img_Brassieresvg-48" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-48">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-48">
											flaticon-381-blueprint</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-blueprint"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-49">
						<div class="svg-icons-prev">
							<i class="flaticon-381-bluetooth"></i>
						</div>
						<div class="svg-classname">flaticon-381-bluetooth</div>

						<div class="modal fade" id="svg_img_Brassieresvg-49" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-49">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-49">
											flaticon-381-bluetooth</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-bluetooth"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-50">
						<div class="svg-icons-prev">
							<i class="flaticon-381-bluetooth-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-bluetooth-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-50" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-50">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-50">
											flaticon-381-bluetooth-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-bluetooth-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-51">
						<div class="svg-icons-prev">
							<i class="flaticon-381-book"></i>
						</div>
						<div class="svg-classname">flaticon-381-book</div>

						<div class="modal fade" id="svg_img_Brassieresvg-51" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-51">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-51">flaticon-381-book
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-book"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-52">
						<div class="svg-icons-prev">
							<i class="flaticon-381-bookmark"></i>
						</div>
						<div class="svg-classname">flaticon-381-bookmark</div>

						<div class="modal fade" id="svg_img_Brassieresvg-52" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-52">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-52">flaticon-381-bookmark
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-bookmark"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-53">
						<div class="svg-icons-prev">
							<i class="flaticon-381-bookmark-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-bookmark-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-53" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-53">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-53">
											flaticon-381-bookmark-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-bookmark-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-54">
						<div class="svg-icons-prev">
							<i class="flaticon-381-box"></i>
						</div>
						<div class="svg-classname">flaticon-381-box</div>

						<div class="modal fade" id="svg_img_Brassieresvg-54" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-54">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-54">flaticon-381-box</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-box"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-55">
						<div class="svg-icons-prev">
							<i class="flaticon-381-box-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-box-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-55" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-55">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-55">flaticon-381-box-1
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-box-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-56">
						<div class="svg-icons-prev">
							<i class="flaticon-381-box-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-box-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-56" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-56">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-56">flaticon-381-box-2
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-box-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-57">
						<div class="svg-icons-prev">
							<i class="flaticon-381-briefcase"></i>
						</div>
						<div class="svg-classname">flaticon-381-briefcase</div>

						<div class="modal fade" id="svg_img_Brassieresvg-57" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-57">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-57">
											flaticon-381-briefcase</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-briefcase"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-58">
						<div class="svg-icons-prev">
							<i class="flaticon-381-broken-heart"></i>
						</div>
						<div class="svg-classname">flaticon-381-broken-heart</div>

						<div class="modal fade" id="svg_img_Brassieresvg-58" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-58">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-58">
											flaticon-381-broken-heart</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-broken-heart"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-59">
						<div class="svg-icons-prev">
							<i class="flaticon-381-broken-link"></i>
						</div>
						<div class="svg-classname">flaticon-381-broken-link</div>

						<div class="modal fade" id="svg_img_Brassieresvg-59" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-59">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-59">
											flaticon-381-broken-link</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-broken-link"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-60">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calculator"></i>
						</div>
						<div class="svg-classname">flaticon-381-calculator</div>

						<div class="modal fade" id="svg_img_Brassieresvg-60" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-60">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-60">
											flaticon-381-calculator</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calculator"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-62">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calculator-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-calculator-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-62" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-62">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-62">
											flaticon-381-calculator-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calculator-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-63">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar</div>

						<div class="modal fade" id="svg_img_Brassieresvg-63" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-63">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-63">flaticon-381-calendar
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-64">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-64" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-64">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-64">
											flaticon-381-calendar-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-65">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-65" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-65">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-65">
											flaticon-381-calendar-2</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-66">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-3"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-3</div>

						<div class="modal fade" id="svg_img_Brassieresvg-66" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-66">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-66">
											flaticon-381-calendar-3</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-3"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-67">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-4"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-4</div>

						<div class="modal fade" id="svg_img_Brassieresvg-67" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-67">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-67">
											flaticon-381-calendar-4</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-4"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-68">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-5"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-5</div>

						<div class="modal fade" id="svg_img_Brassieresvg-68" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-68">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-68">
											flaticon-381-calendar-5</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-5"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-69">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-6"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-6</div>

						<div class="modal fade" id="svg_img_Brassieresvg-69" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-69">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-69">
											flaticon-381-calendar-6</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-6"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-70">
						<div class="svg-icons-prev">
							<i class="flaticon-381-calendar-7"></i>
						</div>
						<div class="svg-classname">flaticon-381-calendar-7</div>

						<div class="modal fade" id="svg_img_Brassieresvg-70" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-70">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-70">
											flaticon-381-calendar-7</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-calendar-7"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-71">
						<div class="svg-icons-prev">
							<i class="flaticon-381-clock"></i>
						</div>
						<div class="svg-classname">flaticon-381-clock</div>

						<div class="modal fade" id="svg_img_Brassieresvg-71" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-71">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-71">flaticon-381-clock
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-clock"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-73">
						<div class="svg-icons-prev">
							<i class="flaticon-381-clock-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-clock-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-73" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-73">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-73">flaticon-381-clock-1
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-clock-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-74">
						<div class="svg-icons-prev">
							<i class="flaticon-381-clock-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-clock-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-74" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-74">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-74">flaticon-381-clock-2
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-clock-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-75">
						<div class="svg-icons-prev">
							<i class="flaticon-381-close"></i>
						</div>
						<div class="svg-classname">flaticon-381-close</div>

						<div class="modal fade" id="svg_img_Brassieresvg-75" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-75">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-75">flaticon-381-close
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-close"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-84">
						<div class="svg-icons-prev">
							<i class="flaticon-381-cloud"></i>
						</div>
						<div class="svg-classname">flaticon-381-cloud</div>

						<div class="modal fade" id="svg_img_Brassieresvg-84" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-84">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-84">flaticon-381-cloud
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-cloud"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-85">
						<div class="svg-icons-prev">
							<i class="flaticon-381-cloud-computing"></i>
						</div>
						<div class="svg-classname">flaticon-381-cloud-computing</div>

						<div class="modal fade" id="svg_img_Brassieresvg-85" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-85">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-85">
											flaticon-381-cloud-computing</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-cloud-computing"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-86">
						<div class="svg-icons-prev">
							<i class="flaticon-381-command"></i>
						</div>
						<div class="svg-classname">flaticon-381-command</div>

						<div class="modal fade" id="svg_img_Brassieresvg-86" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-86">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-86">flaticon-381-command
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-command"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-87">
						<div class="svg-icons-prev">
							<i class="flaticon-381-compact-disc"></i>
						</div>
						<div class="svg-classname">flaticon-381-compact-disc</div>

						<div class="modal fade" id="svg_img_Brassieresvg-87" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-87">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-87">
											flaticon-381-compact-disc</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-compact-disc"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-88">
						<div class="svg-icons-prev">
							<i class="flaticon-381-compact-disc-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-compact-disc-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-88" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-88">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-88">
											flaticon-381-compact-disc-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-compact-disc-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-89">
						<div class="svg-icons-prev">
							<i class="flaticon-381-compact-disc-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-compact-disc-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-89" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-89">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-89">
											flaticon-381-compact-disc-2</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-compact-disc-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-90">
						<div class="svg-icons-prev">
							<i class="flaticon-381-compass"></i>
						</div>
						<div class="svg-classname">flaticon-381-compass</div>

						<div class="modal fade" id="svg_img_Brassieresvg-90" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-90">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-90">flaticon-381-compass
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-compass"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-91">
						<div class="svg-icons-prev">
							<i class="flaticon-381-compass-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-compass-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-91" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-91">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-91">
											flaticon-381-compass-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-compass-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-92">
						<div class="svg-icons-prev">
							<i class="flaticon-381-compass-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-compass-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-92" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-92">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-92">
											flaticon-381-compass-2</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-compass-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-107">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls</div>

						<div class="modal fade" id="svg_img_Brassieresvg-107" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-107">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-107">
											flaticon-381-controls</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-108">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-108" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-108">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-108">
											flaticon-381-controls-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-109">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls-2"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls-2</div>

						<div class="modal fade" id="svg_img_Brassieresvg-109" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-109">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-109">
											flaticon-381-controls-2</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls-2"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-110">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls-3"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls-3</div>

						<div class="modal fade" id="svg_img_Brassieresvg-110" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-110">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-110">
											flaticon-381-controls-3</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls-3"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-111">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls-4"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls-4</div>

						<div class="modal fade" id="svg_img_Brassieresvg-111" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-111">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-111">
											flaticon-381-controls-4</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls-4"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-112">
						<div class="svg-icons-prev">
							<i class="flaticon-381-switch-4"></i>
						</div>
						<div class="svg-classname">flaticon-381-switch-4</div>

						<div class="modal fade" id="svg_img_Brassieresvg-112" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-112">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-112">
											flaticon-381-switch-4</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-switch-4"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-113">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls-5"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls-5</div>

						<div class="modal fade" id="svg_img_Brassieresvg-113" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-113">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-113">
											flaticon-381-controls-5</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls-5"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-114">
						<div class="svg-icons-prev">
							<i class="flaticon-381-controls-6"></i>
						</div>
						<div class="svg-classname">flaticon-381-controls-6</div>

						<div class="modal fade" id="svg_img_Brassieresvg-114" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-114">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-114">
											flaticon-381-controls-6</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-controls-6"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-115">
						<div class="svg-icons-prev">
							<i class="flaticon-381-database"></i>
						</div>
						<div class="svg-classname">flaticon-381-database</div>

						<div class="modal fade" id="svg_img_Brassieresvg-115" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-115">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-115">
											flaticon-381-database</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-database"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-116">
						<div class="svg-icons-prev">
							<i class="flaticon-381-database-1"></i>
						</div>
						<div class="svg-classname">flaticon-381-database-1</div>

						<div class="modal fade" id="svg_img_Brassieresvg-116" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-116">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-116">
											flaticon-381-database-1</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-database-1"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-117">
						<div class="svg-icons-prev">
							<i class="flaticon-381-diamond"></i>
						</div>
						<div class="svg-classname">flaticon-381-diamond</div>

						<div class="modal fade" id="svg_img_Brassieresvg-117" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-117">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-117">flaticon-381-diamond
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-diamond"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-118">
						<div class="svg-icons-prev">
							<i class="flaticon-381-diploma"></i>
						</div>
						<div class="svg-classname">flaticon-381-diploma</div>

						<div class="modal fade" id="svg_img_Brassieresvg-118" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-118">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-118">flaticon-381-diploma
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-diploma"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-119">
						<div class="svg-icons-prev">
							<i class="flaticon-381-dislike"></i>
						</div>
						<div class="svg-classname">flaticon-381-dislike</div>

						<div class="modal fade" id="svg_img_Brassieresvg-119" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-119">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-119">flaticon-381-dislike
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-dislike"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-120">
						<div class="svg-icons-prev">
							<i class="flaticon-381-divide"></i>
						</div>
						<div class="svg-classname">flaticon-381-divide</div>

						<div class="modal fade" id="svg_img_Brassieresvg-120" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-120">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-120">flaticon-381-divide
										</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-divide"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-xxl-3 col-md-4 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov style-1" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-121">
						<div class="svg-icons-prev">
							<i class="flaticon-381-earth-globe"></i>
						</div>
						<div class="svg-classname">flaticon-381-earth-globe</div>

						<div class="modal fade" id="svg_img_Brassieresvg-121" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-121">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-121">
											flaticon-381-earth-globe</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;i class="flaticon-381-earth-globe"&gt;&lt;/i&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>