<div class="col-md-6">
    <div class="authincation-content">
        <div class="row no-gutters">
            <div class="col-xl-12">
                <div class="auth-form">
                    <div class="text-center mb-3">
                        <a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'index'] ); ?>"><img src="<?= $this->Url->webroot('/'); ?>images/logo-full-dark.png" alt="/"></a>
                    </div>
                    <h4 class="text-center mb-4">Sign up your account</h4>
                    <form action="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'index'] ); ?>">
                        <div class="form-group mb-3">
                            <label class="form-label">Username</label>
                            <input type="text" class="form-control" placeholder="username">
                        </div>
                        <div class="form-group mb-3">
                            <label class="form-label">Email</label>
                            <input type="email" class="form-control" placeholder="hello@example.com">
                        </div>
                        <div class="form-group mb-3">
                            <label class="form-label">Password</label>
                            <div class="position-relative">
                                <input type="password" id="dz-password" class="form-control" value="Password">
                                <span class="show-pass eye">
                                    <i class="fa fa-eye-slash"></i>
                                    <i class="fa fa-eye"></i>
                                </span>
                            </div>
                        </div>
                        <div class="text-center mt-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
                        </div>
                    </form>
                    <div class="new-account mt-3">
                        <p class="">Already have an account? <a class="text-primary" href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'page_login'] ); ?>">Sign in</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>