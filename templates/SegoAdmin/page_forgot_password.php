<div class="col-md-6">
    <div class="authincation-content">
        <div class="row no-gutters">
            <div class="col-xl-12">
                <div class="auth-form">
                    <div class="text-center mb-3">
                        <a href="<?= $this->Url->build(['controller' => 'SegoAdmin', 'action' => 'index']); ?>"><img
                                src="<?= $this->Url->webroot('/'); ?>images/logo-full-dark.png" alt="/"></a>
                    </div>
                    <h4 class="text-center mb-4">Forgot Password</h4>
                    <form action="<?= $this->Url->build(['controller' => 'SegoAdmin', 'action' => 'index']); ?>">
                        <div class="form-group mb-3">
                            <label class="form-label">Email</label>
                            <input type="email" class="form-control" value="hello@example.com">
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>