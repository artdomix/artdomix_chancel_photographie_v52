<div class="container-fluid">
	<div class="page-titles">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Email</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Compose</a></li>
		</ol>
	</div>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-xl-3 email-left-body">
							<div class="email-left-box">
								<div class="p-0">
									<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'email_compose'] ); ?>" class="btn btn-primary btn-block">Compose</a>
								</div>
								<div class="mail-list mt-4">
									<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'email_inbox'] ); ?>" class="list-group-item active"><i
											class="fa fa-inbox font-18 align-middle me-2"></i> Inbox <span
											class="badge badge-secondary badge-sm text-white float-end">198</span> </a>
									<a href="javascript:void(0);" class="list-group-item"><i
											class="fa fa-paper-plane font-18 align-middle me-2"></i>Sent</a> <a
										href="javascript:void(0);" class="list-group-item"><i
											class="fa fa-star font-18 align-middle me-2"></i>Important <span
											class="badge badge-danger text-white badge-sm float-end">47</span>
									</a>
									<a href="javascript:void(0);" class="list-group-item"><i
											class="mdi mdi-file-document-box font-18 align-middle me-2"></i>Draft</a><a
										href="javascript:void(0);" class="list-group-item"><i
											class="fa fa-trash font-18 align-middle me-2"></i>Trash</a>
								</div>
								<div class="intro-title d-flex justify-content-between">
									<h5>Categories</h5>
								</div>
								<div class="mail-list mt-4">
									<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'email_inbox'] ); ?>" class="list-group-item"><span class="icon-warning"><i
												class="fa fa-circle" aria-hidden="true"></i></span>
										Work </a>
									<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'email_inbox'] ); ?>" class="list-group-item"><span class="icon-primary"><i
												class="fa fa-circle" aria-hidden="true"></i></span>
										Private </a>
									<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'email_inbox'] ); ?>" class="list-group-item"><span class="icon-success"><i
												class="fa fa-circle" aria-hidden="true"></i></span>
										Support </a>
									<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'email_inbox'] ); ?>" class="list-group-item"><span class="icon-dpink"><i
												class="fa fa-circle" aria-hidden="true"></i></span>
										Social </a>
								</div>
							</div>
						</div>
						<div class="col-xl-9">
							<div class="email-right-box">
								<div class="email-tools-box float-end mb-2">
									<i class="fa-solid fa-list-ul"></i>
								</div>
								<div class="compose-content">
									<form action="#">
										<div class="mb-3">
											<input type="text" class="form-control bg-transparent" placeholder=" To:">
										</div>
										<div class="mb-3">
											<input type="text" class="form-control bg-transparent"
												placeholder=" Subject:">
										</div>
										<div class="mb-3">
											<textarea id="email-compose-editor"
												class="textarea_editor form-control bg-transparent" rows="15"
												placeholder="Enter text ..."></textarea>
										</div>
									</form>
									<h5 class="mb-4"><i class="fa fa-paperclip"></i> Attatchment</h5>
									<form action="#" class="dropzone">
										<div class="fallback">
											<input name="file" type="file" multiple>
										</div>
									</form>

								</div>
								<div class="text-start mt-4 mb-2">
									<button class="btn btn-danger light btn-sl-sm" type="button"><span class="me-2"><i
												class="fa fa-times" aria-hidden="true"></i></span>Discard</button>
									<button class="btn btn-primary btn-sl-sm me-2" type="button"><span class="me-2"><i
												class="fa fa-paper-plane"></i></span>Send</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->Html->scriptStart(['block' => 'script']) ?>
	$(".fa.fa-star").click(function () {
		$(this).toggleClass("yellow");
	});

	$(".email-tools-box").on('click', function () {
		$(' .email-left-body ,.email-tools-box').toggleClass("active");
	});
<?php $this->Html->scriptEnd() ?>