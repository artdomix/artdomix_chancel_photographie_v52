<div class="media p-0 mb-4 alert alert-dismissible items-list-2 border-0">
	<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>"><img class="img-fluid rounded me-3" width="85" src="<?= $this->Url->webroot('/'); ?>images/dish/pic2.jpg" alt="DexignZone"></a>
	<div class="media-body col-6 px-0">
		<h5 class="mt-0 mb-1"><a class="text-black" href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Tuna soup spinach with himalaya salt</a></h5>
		<small class="font-w500 mb-3"><a class="text-primary" href="javascript:void(0);"> JUICE</a></small>
		<span class="text-secondary me-2 fo"></span>
		<ul class="fs-14 list-inline">
			<li class="me-3">Serves for 4 Person</li>
			<li>24mins</li>
		</ul>
	</div>
	<div class="media-footer align-self-center ms-auto d-block align-items-center d-sm-flex">
		<h3 class="mb-0 font-w600 text-secondary">$8.15</h3>
		<div class="dropdown ms-3 ">
			<button type="button" class="btn btn-secondary sharp tp-btn-light " data-bs-toggle="dropdown">
				<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
			</button>
			<div class="dropdown-menu dropdown-menu-end">
				<a class="dropdown-item" href="javascript:void(0);">Edit</a>
				<a href="javascript:void(0);" data-bs-dismiss="alert" aria-label="Close" class="dropdown-item">Delete</a>
			</div>
		</div>
	</div>
</div>
<div class="media p-0 mb-4 alert alert-dismissible items-list-2 border-0">
	<a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>"><img class="img-fluid rounded me-3" width="85" src="<?= $this->Url->webroot('/'); ?>images/dish/pic1.jpg" alt="DexignZone"></a>
	<div class="media-body col-6 px-0">
		<h5 class="mt-0 mb-1"><a class="text-black" href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Watermelon juice with ice</a></h5>
		<small class="font-w500 mb-3"><a class="text-primary" href="javascript:void(0);">PIZZA</a></small>
		<span class="text-secondary me-2 fo"></span>
		<ul class="fs-14 list-inline">
			<li class="me-3">Serves for 4 Person</li>
			<li>24mins</li>
		</ul>
	</div>
	<div class="media-footer align-self-center ms-auto d-block align-items-center d-sm-flex">
		<h3 class="mb-0 font-w600 text-secondary">$5.67</h3>
		<div class="dropdown ms-3 ">
			<button type="button" class="btn btn-secondary sharp tp-btn-light " data-bs-toggle="dropdown">
				<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
			</button>
			<div class="dropdown-menu dropdown-menu-end">
				<a class="dropdown-item" href="javascript:void(0);">Edit</a>
				<a href="javascript:void(0);" data-bs-dismiss="alert" aria-label="Close" class="dropdown-item">Delete</a>
			</div>
		</div>
	</div>
</div>