<div class="container-fluid">
	<div class="page-titles">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Shop</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Product Grid</a></li>
		</ol>
	</div>
	<div class="row">
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/1.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Vegies Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa-solid fa-star-half-stroke"></i></li>
								<li><i class="fa-solid fa-star-half-stroke"></i></li>
							</ul>
							<span class="price">$761.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/2.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Cheese Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
							</ul>
							<span class="price">$159.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/3.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Tomato Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
							</ul>
							<span class="price">$357.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/4.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Pasta Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa-solid fa-star-half-stroke"></i></li>
								<li><i class="fa-solid fa-star-half-stroke"></i></li>
							</ul>
							<span class="price">$654.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/5.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Chilli Pepper Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
							</ul>
							<span class="price">$369.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/6.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Cashew Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
							</ul>
							<span class="price">$245.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/7.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">OTC Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
							</ul>
							<span class="price">$364.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 col-sm-6">
			<div class="card">
				<div class="card-body">
					<div class="new-arrival-product">
						<div class="new-arrivals-img-contnent">
							<img class="img-fluid" src="<?= $this->Url->webroot('/'); ?>images/product/1.jpg" alt="/">
						</div>
						<div class="new-arrival-content text-center mt-3">
							<h4><a href="<?= $this->Url->build( ['controller' => 'SegoAdmin','action' => 'ecom_product_detail'] ); ?>">Double Cheese Pizza</a></h4>
							<ul class="star-rating">
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa fa-star"></i></li>
								<li><i class="fa-solid fa-star-half-stroke"></i></li>
								<li><i class="fa-solid fa-star-half-stroke"></i></li>
							</ul>
							<span class="price">$548.00</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>