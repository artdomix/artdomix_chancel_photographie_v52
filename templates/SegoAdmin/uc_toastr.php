<div class="container-fluid">
	<!-- Add Order -->
	<div class="modal fade" id="addOrderModalside">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Menus</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal">
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group mb-3">
							<label class="form-label">Food Name</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group mb-3">
							<label class="form-label">Order Date</label>
							<input class="form-control" type="text" id="datepicker">
						</div>
						<div class="form-group mb-3">
							<label class="form-label">Food Price</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group mb-3">
							<button type="button" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="page-titles">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Plugins</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Toastr</a></li>
		</ol>
	</div>
	<!-- row -->

	<!-- Toastr -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Toastr</h4>
				</div>
				<div class="card-body">
					<button type="button" class="btn btn-dark mb-2 me-2" id="toastr-success-top-right">Top
						Right</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-bottom-right">Bottom
						Right</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-bottom-left">Bottom
						Left</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-top-left">Top
						Left</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-top-full-width">Top Full
						Width</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-bottom-full-width">Bottom
						Full Width</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-top-center">Top
						Center</button>
					<button type="button" class="btn btn-dark mb-2  me-2" id="toastr-success-bottom-center">Bottom
						Center</button>
					<button type="button" class="btn btn-info mb-2  me-2" id="toastr-info-top-right">Info</button>
					<button type="button" class="btn btn-warning mb-2  me-2"
						id="toastr-warning-top-right">Warning</button>
					<button type="button" class="btn btn-danger mb-2  me-2" id="toastr-danger-top-right">Error</button>
				</div>
			</div>
		</div>
	</div>
</div>