<div class="container-fluid">
	<!-- Add Order -->
	
	<div class="page-titles">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Icons</a></li>
			<li class="breadcrumb-item active"><a href="javascript:void(0)">Svg Icons </a></li>
		</ol>
	</div>
	<!-- row -->
	<div class="card">
		<div class="card-body svg-area">
			<div class="row">
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg width="28" height="28" viewBox="0 0 28 28" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M22.75 15.8385V13.0463C22.7471 10.8855 21.9385 8.80353 20.4821 7.20735C19.0258 5.61116 17.0264 4.61555 14.875 4.41516V2.625C14.875 2.39294 14.7828 2.17038 14.6187 2.00628C14.4546 1.84219 14.2321 1.75 14 1.75C13.7679 1.75 13.5454 1.84219 13.3813 2.00628C13.2172 2.17038 13.125 2.39294 13.125 2.625V4.41534C10.9736 4.61572 8.97429 5.61131 7.51794 7.20746C6.06159 8.80361 5.25291 10.8855 5.25 13.0463V15.8383C4.26257 16.0412 3.37529 16.5784 2.73774 17.3593C2.10019 18.1401 1.75134 19.1169 1.75 20.125C1.75076 20.821 2.02757 21.4882 2.51969 21.9803C3.01181 22.4724 3.67904 22.7492 4.375 22.75H9.71346C9.91521 23.738 10.452 24.6259 11.2331 25.2636C12.0142 25.9013 12.9916 26.2497 14 26.2497C15.0084 26.2497 15.9858 25.9013 16.7669 25.2636C17.548 24.6259 18.0848 23.738 18.2865 22.75H23.625C24.321 22.7492 24.9882 22.4724 25.4803 21.9803C25.9724 21.4882 26.2492 20.821 26.25 20.125C26.2486 19.117 25.8998 18.1402 25.2622 17.3594C24.6247 16.5786 23.7374 16.0414 22.75 15.8385ZM7 13.0463C7.00232 11.2113 7.73226 9.45223 9.02974 8.15474C10.3272 6.85726 12.0863 6.12732 13.9212 6.125H14.0788C15.9137 6.12732 17.6728 6.85726 18.9703 8.15474C20.2677 9.45223 20.9977 11.2113 21 13.0463V15.75H7V13.0463ZM14 24.5C13.4589 24.4983 12.9316 24.3292 12.4905 24.0159C12.0493 23.7026 11.716 23.2604 11.5363 22.75H16.4637C16.284 23.2604 15.9507 23.7026 15.5095 24.0159C15.0684 24.3292 14.5411 24.4983 14 24.5ZM23.625 21H4.375C4.14298 20.9999 3.9205 20.9076 3.75644 20.7436C3.59237 20.5795 3.50014 20.357 3.5 20.125C3.50076 19.429 3.77757 18.7618 4.26969 18.2697C4.76181 17.7776 5.42904 17.5008 6.125 17.5H21.875C22.571 17.5008 23.2382 17.7776 23.7303 18.2697C24.2224 18.7618 24.4992 19.429 24.5 20.125C24.4999 20.357 24.4076 20.5795 24.2436 20.7436C24.0795 20.9076 23.857 20.9999 23.625 21Z"
									fill="#4C8147" />
							</svg>
						</div>
						<div class="svg-classname">bell.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>

						</div>
						<div class="modal fade" id="svg_img_Brassieresvg" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg">bell.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/bell.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg">bell.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"&gt;
&lt;path d="M22.75 15.8385V13.0463C22.7471 10.8855 21.9385 8.80353 20.4821 7.20735C19.0258 5.61116 17.0264 4.61555 14.875 4.41516V2.625C14.875 2.39294 14.7828 2.17038 14.6187 2.00628C14.4546 1.84219 14.2321 1.75 14 1.75C13.7679 1.75 13.5454 1.84219 13.3813 2.00628C13.2172 2.17038 13.125 2.39294 13.125 2.625V4.41534C10.9736 4.61572 8.97429 5.61131 7.51794 7.20746C6.06159 8.80361 5.25291 10.8855 5.25 13.0463V15.8383C4.26257 16.0412 3.37529 16.5784 2.73774 17.3593C2.10019 18.1401 1.75134 19.1169 1.75 20.125C1.75076 20.821 2.02757 21.4882 2.51969 21.9803C3.01181 22.4724 3.67904 22.7492 4.375 22.75H9.71346C9.91521 23.738 10.452 24.6259 11.2331 25.2636C12.0142 25.9013 12.9916 26.2497 14 26.2497C15.0084 26.2497 15.9858 25.9013 16.7669 25.2636C17.548 24.6259 18.0848 23.738 18.2865 22.75H23.625C24.321 22.7492 24.9882 22.4724 25.4803 21.9803C25.9724 21.4882 26.2492 20.821 26.25 20.125C26.2486 19.117 25.8998 18.1402 25.2622 17.3594C24.6247 16.5786 23.7374 16.0414 22.75 15.8385ZM7 13.0463C7.00232 11.2113 7.73226 9.45223 9.02974 8.15474C10.3272 6.85726 12.0863 6.12732 13.9212 6.125H14.0788C15.9137 6.12732 17.6728 6.85726 18.9703 8.15474C20.2677 9.45223 20.9977 11.2113 21 13.0463V15.75H7V13.0463ZM14 24.5C13.4589 24.4983 12.9316 24.3292 12.4905 24.0159C12.0493 23.7026 11.716 23.2604 11.5363 22.75H16.4637C16.284 23.2604 15.9507 23.7026 15.5095 24.0159C15.0684 24.3292 14.5411 24.4983 14 24.5ZM23.625 21H4.375C4.14298 20.9999 3.9205 20.9076 3.75644 20.7436C3.59237 20.5795 3.50014 20.357 3.5 20.125C3.50076 19.429 3.77757 18.7618 4.26969 18.2697C4.76181 17.7776 5.42904 17.5008 6.125 17.5H21.875C22.571 17.5008 23.2382 17.7776 23.7303 18.2697C24.2224 18.7618 24.4992 19.429 24.5 20.125C24.4999 20.357 24.4076 20.5795 24.2436 20.7436C24.0795 20.9076 23.857 20.9999 23.625 21Z" fill="#4C8147"/&gt;
&lt;/svg&gt;</pre>

									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg width="28" height="28" viewBox="0 0 28 28" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M22.4604 3.84863H5.31685C4.64745 3.84937 4.00568 4.11561 3.53234 4.58895C3.059 5.06229 2.79276 5.70406 2.79202 6.37346V18.156C2.79276 18.8254 3.059 19.4672 3.53234 19.9405C4.00568 20.4138 4.64745 20.6801 5.31685 20.6808C5.54002 20.6809 5.75401 20.7697 5.91181 20.9275C6.06961 21.0853 6.15832 21.2993 6.15846 21.5224V23.3166C6.15846 23.6212 6.24115 23.9202 6.39771 24.1815C6.55427 24.4429 6.77882 24.6569 7.04744 24.8006C7.31605 24.9444 7.61864 25.0125 7.92295 24.9978C8.22726 24.9831 8.52186 24.8861 8.77536 24.7171L14.6173 20.8222C14.7554 20.7297 14.9179 20.6805 15.0841 20.6808H19.187C19.7383 20.6798 20.2743 20.4991 20.7137 20.1662C21.1531 19.8332 21.472 19.3661 21.6222 18.8357L24.8965 7.04986C24.9999 6.67457 25.0152 6.2805 24.9413 5.89831C24.8675 5.51613 24.7064 5.15615 24.4707 4.84638C24.235 4.53662 23.9309 4.28544 23.5823 4.11238C23.2336 3.93933 22.8497 3.84907 22.4604 3.84863ZM23.2733 6.6028L20.0006 18.3845C19.95 18.5612 19.8432 18.7166 19.6964 18.8272C19.5496 18.9378 19.3708 18.9976 19.187 18.9976H15.0841C14.5856 18.997 14.0981 19.1446 13.6836 19.4217L7.84168 23.3166V21.5224C7.84094 20.853 7.5747 20.2113 7.10136 19.7379C6.62802 19.2646 5.98625 18.9983 5.31685 18.9976C5.09368 18.9975 4.87969 18.9088 4.72189 18.7509C4.56409 18.5931 4.47537 18.3792 4.47524 18.156V6.37346C4.47537 6.15029 4.56409 5.9363 4.72189 5.7785C4.87969 5.6207 5.09368 5.53198 5.31685 5.53185H22.4604C22.5905 5.53218 22.7188 5.56252 22.8352 5.62052C22.9517 5.67851 23.0532 5.76259 23.1318 5.86621C23.2105 5.96984 23.2641 6.09021 23.2887 6.21797C23.3132 6.34572 23.3079 6.47742 23.2733 6.6028Z"
									fill="#4C8147" />
								<path
									d="M7.84167 11.423H12.0497C12.2729 11.423 12.487 11.3343 12.6448 11.1765C12.8027 11.0186 12.8913 10.8046 12.8913 10.5814C12.8913 10.3581 12.8027 10.1441 12.6448 9.98625C12.487 9.82842 12.2729 9.73975 12.0497 9.73975H7.84167C7.61846 9.73975 7.4044 9.82842 7.24656 9.98625C7.08873 10.1441 7.00006 10.3581 7.00006 10.5814C7.00006 10.8046 7.08873 11.0186 7.24656 11.1765C7.4044 11.3343 7.61846 11.423 7.84167 11.423Z"
									fill="#4C8147" />
								<path
									d="M15.4162 13.1064H7.84167C7.61846 13.1064 7.4044 13.1951 7.24656 13.3529C7.08873 13.5108 7.00006 13.7248 7.00006 13.9481C7.00006 14.1713 7.08873 14.3853 7.24656 14.5432C7.4044 14.701 7.61846 14.7897 7.84167 14.7897H15.4162C15.6394 14.7897 15.8534 14.701 16.0113 14.5432C16.1691 14.3853 16.2578 14.1713 16.2578 13.9481C16.2578 13.7248 16.1691 13.5108 16.0113 13.3529C15.8534 13.1951 15.6394 13.1064 15.4162 13.1064Z"
									fill="#4C8147" />
							</svg>
						</div>
						<div class="svg-classname">message.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-1"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-1"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>

						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-1" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-1">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-1">message.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/message.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-1" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-1">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-1">message.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>

&lt;svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"&gt;
&lt;path d="M22.4604 3.84863H5.31685C4.64745 3.84937 4.00568 4.11561 3.53234 4.58895C3.059 5.06229 2.79276 5.70406 2.79202 6.37346V18.156C2.79276 18.8254 3.059 19.4672 3.53234 19.9405C4.00568 20.4138 4.64745 20.6801 5.31685 20.6808C5.54002 20.6809 5.75401 20.7697 5.91181 20.9275C6.06961 21.0853 6.15832 21.2993 6.15846 21.5224V23.3166C6.15846 23.6212 6.24115 23.9202 6.39771 24.1815C6.55427 24.4429 6.77882 24.6569 7.04744 24.8006C7.31605 24.9444 7.61864 25.0125 7.92295 24.9978C8.22726 24.9831 8.52186 24.8861 8.77536 24.7171L14.6173 20.8222C14.7554 20.7297 14.9179 20.6805 15.0841 20.6808H19.187C19.7383 20.6798 20.2743 20.4991 20.7137 20.1662C21.1531 19.8332 21.472 19.3661 21.6222 18.8357L24.8965 7.04986C24.9999 6.67457 25.0152 6.2805 24.9413 5.89831C24.8675 5.51613 24.7064 5.15615 24.4707 4.84638C24.235 4.53662 23.9309 4.28544 23.5823 4.11238C23.2336 3.93933 22.8497 3.84907 22.4604 3.84863ZM23.2733 6.6028L20.0006 18.3845C19.95 18.5612 19.8432 18.7166 19.6964 18.8272C19.5496 18.9378 19.3708 18.9976 19.187 18.9976H15.0841C14.5856 18.997 14.0981 19.1446 13.6836 19.4217L7.84168 23.3166V21.5224C7.84094 20.853 7.5747 20.2113 7.10136 19.7379C6.62802 19.2646 5.98625 18.9983 5.31685 18.9976C5.09368 18.9975 4.87969 18.9088 4.72189 18.7509C4.56409 18.5931 4.47537 18.3792 4.47524 18.156V6.37346C4.47537 6.15029 4.56409 5.9363 4.72189 5.7785C4.87969 5.6207 5.09368 5.53198 5.31685 5.53185H22.4604C22.5905 5.53218 22.7188 5.56252 22.8352 5.62052C22.9517 5.67851 23.0532 5.76259 23.1318 5.86621C23.2105 5.96984 23.2641 6.09021 23.2887 6.21797C23.3132 6.34572 23.3079 6.47742 23.2733 6.6028Z" fill="#4C8147"/&gt;
&lt;path d="M7.84167 11.423H12.0497C12.2729 11.423 12.487 11.3343 12.6448 11.1765C12.8027 11.0186 12.8913 10.8046 12.8913 10.5814C12.8913 10.3581 12.8027 10.1441 12.6448 9.98625C12.487 9.82842 12.2729 9.73975 12.0497 9.73975H7.84167C7.61846 9.73975 7.4044 9.82842 7.24656 9.98625C7.08873 10.1441 7.00006 10.3581 7.00006 10.5814C7.00006 10.8046 7.08873 11.0186 7.24656 11.1765C7.4044 11.3343 7.61846 11.423 7.84167 11.423Z" fill="#4C8147"/&gt;
&lt;path d="M15.4162 13.1064H7.84167C7.61846 13.1064 7.4044 13.1951 7.24656 13.3529C7.08873 13.5108 7.00006 13.7248 7.00006 13.9481C7.00006 14.1713 7.08873 14.3853 7.24656 14.5432C7.4044 14.701 7.61846 14.7897 7.84167 14.7897H15.4162C15.6394 14.7897 15.8534 14.701 16.0113 14.5432C16.1691 14.3853 16.2578 14.1713 16.2578 13.9481C16.2578 13.7248 16.1691 13.5108 16.0113 13.3529C15.8534 13.1951 15.6394 13.1064 15.4162 13.1064Z" fill="#4C8147"/&gt;
&lt;/svg&gt;
												</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"
								fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round">
								<path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
								<polyline points="16 17 21 12 16 7"></polyline>
								<line x1="21" y1="12" x2="9" y2="12"></line>
							</svg>
						</div>
						<div class="svg-classname">logout.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-2"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-2"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-2" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-2">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-2">logout.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/logout.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-2" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-2">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-2">logout.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"&gt;&lt;path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"&gt;&lt;/path&gt;&lt;polyline points="16 17 21 12 16 7"&gt;&lt;/polyline&gt;&lt;line x1="21" y1="12" x2="9" y2="12"&gt;&lt;/line&gt;&lt;/svg&gt;
													</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"
								fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round">
								<path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z">
								</path>
								<polyline points="22,6 12,13 2,6"></polyline>
							</svg>
						</div>
						<div class="svg-classname">mail.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-23"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-23"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-23" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-23">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-23">mail.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/mail.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-23" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-23">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-23">mail.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"&gt;&lt;path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"&gt;&lt;/path&gt;&lt;polyline points="22,6 12,13 2,6"&gt;&lt;/polyline&gt;&lt;/svg&gt;
</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"
								fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round">
								<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
								<circle cx="12" cy="7" r="4"></circle>
							</svg>
						</div>
						<div class="svg-classname">user.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-24"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-24"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-24" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-24">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-24">user.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/user.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-24" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-24">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-24">user.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"&gt;&lt;path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"&gt;&lt;/path&gt;&lt;circle cx="12" cy="7" r="4"&gt;&lt;/circle&gt;&lt;/svg&gt;
												</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg width="28" height="28" viewBox="0 0 28 28" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M23.625 6.12494H22.75V2.62494C22.75 2.47256 22.7102 2.32283 22.6345 2.19056C22.5589 2.05829 22.45 1.94807 22.3186 1.87081C22.1873 1.79355 22.0381 1.75193 21.8857 1.75007C21.7333 1.7482 21.5831 1.78616 21.4499 1.86019L14 5.99902L6.55007 1.86019C6.41688 1.78616 6.26667 1.7482 6.11431 1.75007C5.96194 1.75193 5.8127 1.79355 5.68136 1.87081C5.55002 1.94807 5.44113 2.05829 5.36547 2.19056C5.28981 2.32283 5.25001 2.47256 5.25 2.62494V6.12494H4.375C3.67904 6.1257 3.01181 6.40251 2.51969 6.89463C2.02757 7.38674 1.75076 8.05398 1.75 8.74994V11.3749C1.75076 12.0709 2.02757 12.7381 2.51969 13.2302C3.01181 13.7224 3.67904 13.9992 4.375 13.9999H5.25V23.6249C5.25076 24.3209 5.52757 24.9881 6.01969 25.4802C6.51181 25.9724 7.17904 26.2492 7.875 26.2499H20.125C20.821 26.2492 21.4882 25.9724 21.9803 25.4802C22.4724 24.9881 22.7492 24.3209 22.75 23.6249V13.9999H23.625C24.321 13.9992 24.9882 13.7224 25.4803 13.2302C25.9724 12.7381 26.2492 12.0709 26.25 11.3749V8.74994C26.2492 8.05398 25.9724 7.38674 25.4803 6.89463C24.9882 6.40251 24.321 6.1257 23.625 6.12494ZM21 6.12494H17.3769L21 4.11244V6.12494ZM7 4.11244L10.6231 6.12494H7V4.11244ZM7 23.6249V13.9999H13.125V24.4999H7.875C7.64303 24.4996 7.42064 24.4073 7.25661 24.2433C7.09258 24.0793 7.0003 23.8569 7 23.6249ZM21 23.6249C20.9997 23.8569 20.9074 24.0793 20.7434 24.2433C20.5794 24.4073 20.357 24.4996 20.125 24.4999H14.875V13.9999H21V23.6249ZM24.5 11.3749C24.4997 11.6069 24.4074 11.8293 24.2434 11.9933C24.0794 12.1574 23.857 12.2496 23.625 12.2499H4.375C4.14303 12.2496 3.92064 12.1574 3.75661 11.9933C3.59258 11.8293 3.5003 11.6069 3.5 11.3749V8.74994C3.5003 8.51796 3.59258 8.29558 3.75661 8.13155C3.92064 7.96752 4.14303 7.87524 4.375 7.87494H23.625C23.857 7.87524 24.0794 7.96752 24.2434 8.13155C24.4074 8.29558 24.4997 8.51796 24.5 8.74994V11.3749Z"
									fill="#4C8147" />
							</svg>
						</div>
						<div class="svg-classname">gift.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-25"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-25"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-25" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-25">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-25">gift.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/gift.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-25" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-25">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-25">gift.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"&gt;
&lt;path d="M23.625 6.12494H22.75V2.62494C22.75 2.47256 22.7102 2.32283 22.6345 2.19056C22.5589 2.05829 22.45 1.94807 22.3186 1.87081C22.1873 1.79355 22.0381 1.75193 21.8857 1.75007C21.7333 1.7482 21.5831 1.78616 21.4499 1.86019L14 5.99902L6.55007 1.86019C6.41688 1.78616 6.26667 1.7482 6.11431 1.75007C5.96194 1.75193 5.8127 1.79355 5.68136 1.87081C5.55002 1.94807 5.44113 2.05829 5.36547 2.19056C5.28981 2.32283 5.25001 2.47256 5.25 2.62494V6.12494H4.375C3.67904 6.1257 3.01181 6.40251 2.51969 6.89463C2.02757 7.38674 1.75076 8.05398 1.75 8.74994V11.3749C1.75076 12.0709 2.02757 12.7381 2.51969 13.2302C3.01181 13.7224 3.67904 13.9992 4.375 13.9999H5.25V23.6249C5.25076 24.3209 5.52757 24.9881 6.01969 25.4802C6.51181 25.9724 7.17904 26.2492 7.875 26.2499H20.125C20.821 26.2492 21.4882 25.9724 21.9803 25.4802C22.4724 24.9881 22.7492 24.3209 22.75 23.6249V13.9999H23.625C24.321 13.9992 24.9882 13.7224 25.4803 13.2302C25.9724 12.7381 26.2492 12.0709 26.25 11.3749V8.74994C26.2492 8.05398 25.9724 7.38674 25.4803 6.89463C24.9882 6.40251 24.321 6.1257 23.625 6.12494ZM21 6.12494H17.3769L21 4.11244V6.12494ZM7 4.11244L10.6231 6.12494H7V4.11244ZM7 23.6249V13.9999H13.125V24.4999H7.875C7.64303 24.4996 7.42064 24.4073 7.25661 24.2433C7.09258 24.0793 7.0003 23.8569 7 23.6249ZM21 23.6249C20.9997 23.8569 20.9074 24.0793 20.7434 24.2433C20.5794 24.4073 20.357 24.4996 20.125 24.4999H14.875V13.9999H21V23.6249ZM24.5 11.3749C24.4997 11.6069 24.4074 11.8293 24.2434 11.9933C24.0794 12.1574 23.857 12.2496 23.625 12.2499H4.375C4.14303 12.2496 3.92064 12.1574 3.75661 11.9933C3.59258 11.8293 3.5003 11.6069 3.5 11.3749V8.74994C3.5003 8.51796 3.59258 8.29558 3.75661 8.13155C3.92064 7.96752 4.14303 7.87524 4.375 7.87494H23.625C23.857 7.87524 24.0794 7.96752 24.2434 8.13155C24.4074 8.29558 24.4997 8.51796 24.5 8.74994V11.3749Z" fill="#4C8147"/&gt;
&lt;/svg&gt;
												</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg width="20" height="20" viewBox="0 0 30 30" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<g>
									<path
										d="M22.4281 2.856H21.8681V1.428C21.8681 0.56 21.2801 0 20.4401 0C19.6001 0 19.0121 0.56 19.0121 1.428V2.856H9.71606V1.428C9.71606 0.56 9.15606 0 8.28806 0C7.42006 0 6.86006 0.56 6.86006 1.428V2.856H5.57206C2.85606 2.856 0.560059 5.152 0.560059 7.868V23.016C0.560059 25.732 2.85606 28.028 5.57206 28.028H22.4281C25.1441 28.028 27.4401 25.732 27.4401 23.016V7.868C27.4401 5.152 25.1441 2.856 22.4281 2.856ZM5.57206 5.712H22.4281C23.5761 5.712 24.5841 6.72 24.5841 7.868V9.856H3.41606V7.868C3.41606 6.72 4.42406 5.712 5.57206 5.712ZM22.4281 25.144H5.57206C4.42406 25.144 3.41606 24.136 3.41606 22.988V12.712H24.5561V22.988C24.5841 24.136 23.5761 25.144 22.4281 25.144Z"
										fill="#2F4CDD"></path>
								</g>
							</svg>
						</div>
						<div class="svg-classname">calender.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-26"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-26"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-26" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-26">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-26">calender.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/calender.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-26" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-26">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-26">calender.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg width="20" height="20" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"&gt;&lt;g&gt;&lt;path d="M22.4281 2.856H21.8681V1.428C21.8681 0.56 21.2801 0 20.4401 0C19.6001 0 19.0121 0.56 19.0121 1.428V2.856H9.71606V1.428C9.71606 0.56 9.15606 0 8.28806 0C7.42006 0 6.86006 0.56 6.86006 1.428V2.856H5.57206C2.85606 2.856 0.560059 5.152 0.560059 7.868V23.016C0.560059 25.732 2.85606 28.028 5.57206 28.028H22.4281C25.1441 28.028 27.4401 25.732 27.4401 23.016V7.868C27.4401 5.152 25.1441 2.856 22.4281 2.856ZM5.57206 5.712H22.4281C23.5761 5.712 24.5841 6.72 24.5841 7.868V9.856H3.41606V7.868C3.41606 6.72 4.42406 5.712 5.57206 5.712ZM22.4281 25.144H5.57206C4.42406 25.144 3.41606 24.136 3.41606 22.988V12.712H24.5561V22.988C24.5841 24.136 23.5761 25.144 22.4281 25.144Z" fill="#2F4CDD"&gt;&lt;/path&gt;&lt;/g&gt;&lt;/svg&gt;
														</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 28 28"
								fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round">
								<path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
								<polyline points="14 2 14 8 20 8"></polyline>
								<line x1="16" y1="13" x2="8" y2="13"></line>
								<line x1="16" y1="17" x2="8" y2="17"></line>
								<polyline points="10 9 9 9 8 9"></polyline>
							</svg>
						</div>
						<div class="svg-classname">bill.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-27"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-27"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>

						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-27" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-27">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-27">bill.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/bill.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-27" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-27">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-27">bill.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 28 28" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" &gt;
&lt;path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"&gt;&lt;/path&gt;
&lt;polyline points="14 2 14 8 20 8"&gt;&lt;/polyline&gt;
&lt;line x1="16" y1="13" x2="8" y2="13"&gt;&lt;/line&gt;
&lt;line x1="16" y1="17" x2="8" y2="17"&gt;&lt;/line&gt;
&lt;polyline points="10 9 9 9 8 9"&gt;&lt;/polyline&gt;
&lt;/svg&gt;
</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 28 28"
								fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round">
								<line x1="12" y1="1" x2="12" y2="23"></line>
								<path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
							</svg>
							dollor.svg
						</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-28"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-28"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-28" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-28">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-28">dollor.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/dollor.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-28" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-28">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-28">dollor.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 28 28" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" &gt;
&lt;line x1="12" y1="1" x2="12" y2="23"&gt;&lt;/line&gt;
&lt;path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"&gt;&lt;/path&gt;
&lt;/svg&gt;
															</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg id="icon-database-widget" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
								viewBox="0 0 24 24" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round"
								stroke-linejoin="round">
								<ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
								<path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
								<path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
							</svg>
						</div>
						<div class="svg-classname">database.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-31"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-31"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-31" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-31">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-31">database.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/database.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-31" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-31">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-31">database.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg id="icon-database-widget" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#EA7A9A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"&gt;
&lt;ellipse cx="12" cy="5" rx="9" ry="3"&gt;&lt;/ellipse&gt;
&lt;path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"&gt;&lt;/path&gt;
&lt;path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"&gt;&lt;/path&gt;
&lt;/svg&gt;
													</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg viewBox="0 0 24 24" width="80" height="80" stroke="#EA7A9A" stroke-width="1"
								fill="none" stroke-linecap="round" stroke-linejoin="round">
								<path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path>
								<line x1="3" y1="6" x2="21" y2="6"></line>
								<path d="M16 10a4 4 0 0 1-8 0"></path>
							</svg>
						</div>
						<div class="svg-classname">bag.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-32"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-32"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-32" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-32">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-32">bag.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/bag.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-32" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-32">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-32">bag.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg viewBox="0 0 24 24" width="80" height="80" stroke="#EA7A9A" stroke-width="1" fill="none" stroke-linecap="round" stroke-linejoin="round"&gt;
&lt;path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"&gt;&lt;/path&gt;
&lt;line x1="3" y1="6" x2="21" y2="6"&gt;&lt;/line&gt;
&lt;path d="M16 10a4 4 0 0 1-8 0"&gt;&lt;/path&gt;
&lt;/svg&gt;
															</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-xxl-3 col-md-6 col-sm-6 col-12 m-b30">
					<div class="svg-icons-ov">
						<div class="svg-icons-prev">
							<svg width="28" height="28" viewBox="0 0 28 28" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M6.04863 18.6668H5.83366C5.18937 18.6668 4.66699 19.1886 4.66699 19.8335C4.66699 20.4784 5.18937 21.0002 5.83366 21.0002H6.04871C6.53185 22.3552 7.81473 23.3335 9.33366 23.3335C10.8526 23.3335 12.1355 22.3552 12.6187 21.0002H15.382C15.8652 22.3552 17.1481 23.3335 18.667 23.3335C20.1926 23.3335 21.4796 22.3463 21.9577 20.9819C23.3498 20.8887 24.511 19.915 24.8176 18.5347L26.2697 11.9984C26.4782 11.0618 26.2532 10.0946 25.6522 9.34603C25.0512 8.59635 24.1562 8.16683 23.1958 8.16683H21.6708C21.5538 7.73054 21.3433 7.3191 21.0453 6.96257C20.4455 6.24479 19.5648 5.8335 18.6294 5.8335H7.00033C6.35604 5.8335 5.83366 6.35531 5.83366 7.00016C5.83366 7.64502 6.35604 8.16683 7.00033 8.16683H18.6294C18.9609 8.16683 19.1637 8.35026 19.2555 8.45964C19.3466 8.56901 19.4907 8.80029 19.4326 9.125L18.1096 16.3898C16.8387 16.5953 15.8044 17.482 15.382 18.6668H12.6187C12.1355 17.3117 10.8526 16.3335 9.33366 16.3335C7.81473 16.3335 6.53178 17.3117 6.04863 18.6668ZM23.8321 10.8055C23.9238 10.9194 24.0657 11.1598 23.9922 11.4925L22.5401 18.0288C22.4722 18.3337 22.241 18.5553 21.9518 18.6314C21.6688 17.8666 21.1205 17.221 20.4053 16.8071L21.554 10.5002H23.1958C23.5365 10.5002 23.7404 10.6916 23.8321 10.8055ZM18.667 18.6668C18.741 18.6668 18.8157 18.6725 18.8658 18.6805C19.4264 18.7899 19.8337 19.2741 19.8337 19.8335C19.8337 20.4772 19.3101 21.0002 18.667 21.0002C18.0238 21.0002 17.5003 20.4772 17.5003 19.8335C17.5003 19.1898 18.0238 18.6668 18.667 18.6668ZM9.33366 18.6668C9.97681 18.6668 10.5003 19.1898 10.5003 19.8335C10.5003 20.4772 9.97681 21.0002 9.33366 21.0002C8.69051 21.0002 8.16699 20.4772 8.16699 19.8335C8.16699 19.1898 8.69051 18.6668 9.33366 18.6668Z"
									fill="#EA7A9A" />
								<path
									d="M4.66667 12.8333H9.33333C9.97762 12.8333 10.5 12.3115 10.5 11.6667C10.5 11.0218 9.97762 10.5 9.33333 10.5H4.66667C4.02238 10.5 3.5 11.0218 3.5 11.6667C3.5 12.3115 4.02238 12.8333 4.66667 12.8333Z"
									fill="#EA7A9A" />
								<path
									d="M2.33366 16.3333H5.83366C6.47795 16.3333 7.00033 15.8115 7.00033 15.1667C7.00033 14.5218 6.47795 14 5.83366 14H2.33366C1.68937 14 1.16699 14.5218 1.16699 15.1667C1.16699 15.8115 1.68937 16.3333 2.33366 16.3333Z"
									fill="#EA7A9A" />
								<path
									d="M3.49967 8.16683C4.144 8.16683 4.66634 7.64449 4.66634 7.00016C4.66634 6.35583 4.144 5.8335 3.49967 5.8335C2.85534 5.8335 2.33301 6.35583 2.33301 7.00016C2.33301 7.64449 2.85534 8.16683 3.49967 8.16683Z"
									fill="#EA7A9A" />
								<path
									d="M2.33366 20.9998C2.97799 20.9998 3.50033 20.4775 3.50033 19.8332C3.50033 19.1888 2.97799 18.6665 2.33366 18.6665C1.68933 18.6665 1.16699 19.1888 1.16699 19.8332C1.16699 20.4775 1.68933 20.9998 2.33366 20.9998Z"
									fill="#EA7A9A" />
							</svg>
						</div>
						<div class="svg-classname">truck.svg</div>
						<div class="svg-icon-popup">
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_img_Brassieresvg-33"
								class="btn btn-sm btn-brand"><i class="fa-solid fa-image"></i></a>
							<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#svg_inline_Brassieresvg-33"
								class="btn btn-sm btn-brand"><i class="fa fa-code"></i></a>
						</div>
						<div class="modal fade" id="svg_img_Brassieresvg-33" tabindex="-1" role="dialog"
							aria-labelledby="svg_img_label_Brassieresvg-33">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_img_label_Brassieresvg-33">truck.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>&lt;img src="images/iconly/bulk/truck.svg"/&gt;</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="svg_inline_Brassieresvg-33" tabindex="-1" role="dialog"
							aria-labelledby="svg_inline_label_Brassieresvg-33">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="svg_inline_label_Brassieresvg-33">truck.svg</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal"
											aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<pre>
&lt;svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"&gt;
&lt;path d="M6.04863 18.6668H5.83366C5.18937 18.6668 4.66699 19.1886 4.66699 19.8335C4.66699 20.4784 5.18937 21.0002 5.83366 21.0002H6.04871C6.53185 22.3552 7.81473 23.3335 9.33366 23.3335C10.8526 23.3335 12.1355 22.3552 12.6187 21.0002H15.382C15.8652 22.3552 17.1481 23.3335 18.667 23.3335C20.1926 23.3335 21.4796 22.3463 21.9577 20.9819C23.3498 20.8887 24.511 19.915 24.8176 18.5347L26.2697 11.9984C26.4782 11.0618 26.2532 10.0946 25.6522 9.34603C25.0512 8.59635 24.1562 8.16683 23.1958 8.16683H21.6708C21.5538 7.73054 21.3433 7.3191 21.0453 6.96257C20.4455 6.24479 19.5648 5.8335 18.6294 5.8335H7.00033C6.35604 5.8335 5.83366 6.35531 5.83366 7.00016C5.83366 7.64502 6.35604 8.16683 7.00033 8.16683H18.6294C18.9609 8.16683 19.1637 8.35026 19.2555 8.45964C19.3466 8.56901 19.4907 8.80029 19.4326 9.125L18.1096 16.3898C16.8387 16.5953 15.8044 17.482 15.382 18.6668H12.6187C12.1355 17.3117 10.8526 16.3335 9.33366 16.3335C7.81473 16.3335 6.53178 17.3117 6.04863 18.6668ZM23.8321 10.8055C23.9238 10.9194 24.0657 11.1598 23.9922 11.4925L22.5401 18.0288C22.4722 18.3337 22.241 18.5553 21.9518 18.6314C21.6688 17.8666 21.1205 17.221 20.4053 16.8071L21.554 10.5002H23.1958C23.5365 10.5002 23.7404 10.6916 23.8321 10.8055ZM18.667 18.6668C18.741 18.6668 18.8157 18.6725 18.8658 18.6805C19.4264 18.7899 19.8337 19.2741 19.8337 19.8335C19.8337 20.4772 19.3101 21.0002 18.667 21.0002C18.0238 21.0002 17.5003 20.4772 17.5003 19.8335C17.5003 19.1898 18.0238 18.6668 18.667 18.6668ZM9.33366 18.6668C9.97681 18.6668 10.5003 19.1898 10.5003 19.8335C10.5003 20.4772 9.97681 21.0002 9.33366 21.0002C8.69051 21.0002 8.16699 20.4772 8.16699 19.8335C8.16699 19.1898 8.69051 18.6668 9.33366 18.6668Z" fill="#EA7A9A"/&gt;
&lt;path d="M4.66667 12.8333H9.33333C9.97762 12.8333 10.5 12.3115 10.5 11.6667C10.5 11.0218 9.97762 10.5 9.33333 10.5H4.66667C4.02238 10.5 3.5 11.0218 3.5 11.6667C3.5 12.3115 4.02238 12.8333 4.66667 12.8333Z" fill="#EA7A9A"/&gt;
&lt;path d="M2.33366 16.3333H5.83366C6.47795 16.3333 7.00033 15.8115 7.00033 15.1667C7.00033 14.5218 6.47795 14 5.83366 14H2.33366C1.68937 14 1.16699 14.5218 1.16699 15.1667C1.16699 15.8115 1.68937 16.3333 2.33366 16.3333Z" fill="#EA7A9A"/&gt;
&lt;path d="M3.49967 8.16683C4.144 8.16683 4.66634 7.64449 4.66634 7.00016C4.66634 6.35583 4.144 5.8335 3.49967 5.8335C2.85534 5.8335 2.33301 6.35583 2.33301 7.00016C2.33301 7.64449 2.85534 8.16683 3.49967 8.16683Z" fill="#EA7A9A"/&gt;
&lt;path d="M2.33366 20.9998C2.97799 20.9998 3.50033 20.4775 3.50033 19.8332C3.50033 19.1888 2.97799 18.6665 2.33366 18.6665C1.68933 18.6665 1.16699 19.1888 1.16699 19.8332C1.16699 20.4775 1.68933 20.9998 2.33366 20.9998Z" fill="#EA7A9A"/&gt;
&lt;/svg&gt;
															</pre>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger"
											data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>