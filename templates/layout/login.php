<!DOCTYPE html>
<html lang="en" class="h-100">

<head> 
	<title>Sego - CakePHP Restaurant Admin Dashboard Bootstrap HTML Template</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="DexignZone">
	<meta name="robots" content="">

	<meta name="keywords" content="	admin dashboard, admin template, administration, analytics, bootstrap, cafe admin, elegant, food, health, kitchen, modern, responsive admin dashboard, restaurant dashboard, restaurant, chef, customer, breakfast, fastfood, nutrition,">
	<meta name="description" content="Experience the ultimate in restaurant management with Sego - the Restaurant Bootstrap Admin Dashboard. Streamline your restaurant operations, from reservations to menu updates, with this powerful and user-friendly admin tool. Elevate your dining experience today.">

	<meta property="og:title" content="Sego - CakePHP Restaurant Admin Dashboard Bootstrap  Template">
	<meta property="og:description" content="Experience the ultimate in restaurant management with Sego - the CakePHP Restaurant Bootstrap Admin Dashboard. Streamline your restaurant operations, from reservations to menu updates, with this powerful and user-friendly admin tool. Elevate your dining experience today.">
	<meta property="og:image" content="https://sego.dexignzone.com/cakephp/social-image.png">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= $this->Html->css('css/style.css', [ 'class' => 'main-css', 'rel' => 'stylesheet']);?>
	<?= $this->Html->meta('icon', 'images/favicon.png', ['type' => '',]); ?>
</head>
    
<body class="h-100">
    <div class="authincation">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <?= $this->fetch('content') ?>
			</div>
		</div>
	</div>
    
    <?= $this->Html->script('vendor/global/global.min.js'); ?>
    <?= $this->Html->script('vendor/bootstrap-select/dist/js/bootstrap-select.min.js'); ?>
    <?= $this->Html->script('js/custom.js'); ?>
    <?= $this->Html->script('js/deznav-init.js'); ?> 
</body>
</html>	