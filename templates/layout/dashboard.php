<!DOCTYPE html>
<html lang="en">

<head>

<title>Sego - CakePHP Restaurant Admin Dashboard Bootstrap Template</title>

<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="author" content="DexignZone">
<meta name="robots" content="">

<meta name="keywords" content="	admin dashboard, admin template, administration, analytics, bootstrap, cafe admin, elegant, food, health, kitchen, modern, responsive admin dashboard, restaurant dashboard, restaurant, chef, customer, breakfast, fastfood, nutrition,">
<meta name="description" content="Experience the ultimate in restaurant management with Sego - the Restaurant Bootstrap Admin Dashboard. Streamline your restaurant operations, from reservations to menu updates, with this powerful and user-friendly admin tool. Elevate your dining experience today.">

<meta property="og:title" content="Sego - CakePHP Restaurant Admin Dashboard Bootstrap  Template">
<meta property="og:description" content="Experience the ultimate in restaurant management with Sego - the CakePHP Restaurant Bootstrap Admin Dashboard. Streamline your restaurant operations, from reservations to menu updates, with this powerful and user-friendly admin tool. Elevate your dining experience today.">
<meta property="og:image" content="https://sego.dexignzone.com/cakephp/social-image.png">
<meta name="format-detection" content="telephone=no">

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
	<?php 
	$controllerName = $this->request->getParam('controller');
	$actionName = $this->request->getParam('action');
	
	//die;
	echo $this->Html->meta('icon', 'images/favicon.png', ['type' => '',]); 
    
	echo $this->Html->css('vendor/bootstrap-select/dist/css/bootstrap-select.min.css');
	echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
	echo $this->Html->css('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap');

	
	/* ***  Page level css : Dashboard  *** */
	if($actionName == 'index')
	{
		?>
		<!-- Page level css : Dashboard  -->
		<?php
		echo $this->Html->css('vendor/chartist/css/chartist.min.css'); 
		echo $this->Html->css('vendor/chartist/css/chartist.min.css'); 
	
		?>
		<!-- End Page level css : Dashboard  -->
		<?php
	}	

	/* ***  Page level css : Dashboard-2  *** */
	if($actionName == 'index2')
	{
		echo $this->Html->css('vendor/chartist/css/chartist.min.css'); 
		 
	}
	/* ***  End Page level css : Dashboard-2  *** */
	
	/* ***  Page level css : order-list  *** */
	
	if($actionName == 'orders'){
		
		?>
		<!-- Page level css : order-list  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		?>
		<!-- End Page level css : order-list  -->
		<?php
	}
	/* ***  Page level css : general-customers  *** */
	
	if($actionName == 'generalCustomers'){
		
		?>
		<!-- Page level css : general-customers  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		?>
		<!-- End Page level css : general-customers  -->
		<?php
	}
	
	/* *** Page level css : Event  *** */
	
	if($actionName == 'event')
	{
		?>
		<!-- Page level css : Event  -->
		<?php
		echo $this->Html->css('vendor/chartist/css/chartist.min.css');
		echo $this->Html->css('vendor/owl-carousel/owl.carousel.css');
		?>
		<!-- End  Page level css : Event -->
		<?php
	}

	/* *** Page level css : customer-list  *** */
	
	if($actionName == 'customer')
	{
		?>
		<!-- Page level css : customer  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		?>
		<!-- End  Page level css : customer-list  -->
		<?php
	}
	
	/* *** Page level css : analytics *** */
	
	if($actionName == 'analytics')
	{
		?>
		<!-- Page level css : analytics  -->
		<?php
		echo $this->Html->css('vendor/chartist/css/chartist.min.css');
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		?>
		<!-- End Page level css : analytics  -->
		<?php
	}
	
	/* ***  Page level css :reviews  *** */
	
	if($actionName == 'reviews')
	{
		?>
		<!-- Page level css : reviews  -->
		<?php
		echo $this->Html->css('vendor/owl-carousel/owl.carousel.css');
		?>
		<!-- End Page level css : reviews  -->
		<?php
	}	
	
	/* *** Page level css :formEditor  *** */
	
	if($actionName == 'formEditor')
	{
		?>
		<!-- Page level css : formEditor -->
		<?php
		echo $this->Html->css('vendor/summernote/summernote.css'); 
		?>

	<!-- End Page level css : project  -->
		<?php
	}
	if($actionName == 'project')
	{
		?>
		<!-- Page level css : project -->
		<?php
		echo $this->Html->css('vendor/tagify/dist/tagify.css'); 
		?>
		<!-- End Page level css : project  -->
		<?php
	}
	
	
	/* *** Page level css :app-profile  *** */
	
	if($actionName == 'appProfile')
	{
		?>
		<!-- Page level css :app-profile  -->
		<?php
		echo $this->Html->css('vendor/lightgallery/dist/css/lightgallery.css');
		echo $this->Html->css('vendor/lightgallery/dist/css/lg-thumbnail.css');
		echo $this->Html->css('vendor/lightgallery/dist/css/lg-zoom.css');
		?>
		<!-- End Page level css : app-profile  -->
		<?php
	}
	/* *** Page level css :-summernote  *** */
	
	if($actionName == 'formEditorSummernote')
	{
		?>
		<!-- Page level css :summernote  -->
		<?php
		echo $this->Html->css('vendor/summernote/summernote.css');
		?>
		<!-- End Page level cs : summernote  -->
		<?php
	}

	/* *** Page level css :customer-profile  *** */
	if($actionName == 'customerProfile')
	{
		?>
		<!-- Page level css :customer-profile  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		echo $this->Html->css('vendor/tagify/dist/tagify.css');
		?>
		<!-- End Page level css : customer-profile  -->
		<?php
	}

	/* *** Page level css :contacts  *** */
	if($actionName == 'contacts')
	{
		?>
		<!-- Page level css :contacts  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		echo $this->Html->css('vendor/tagify/dist/tagify.css');
		?>
		<!-- End Page level css : contacts  -->
		<?php
	}

	/* *** Page level css :add-blog  *** */
	
	if($actionName == 'addBlog')
	{
		?>
		 <!-- Page level script :add-blog  -->
		<?php
		echo $this->Html->css('vendor/select2/css/select2.min.css');
	
		
	
		?>
		<!-- End Page level css :add-blog  -->
		<?php
	}


	/* *** Page level css :reports  *** */
	
	if($actionName == 'reports')
	{
		?>
		<!-- Page level css :reports  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css');
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		echo $this->Html->css('vendor/tagify/dist/tagify.css');
		?>
		<!-- End Page level css : reports  -->
		<?php
	}
	
	/* *** Page level css :manage-client  *** */
	
	if($actionName == 'manageClient')
	{
		?>
		<!-- Page level css :manage-client  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css');
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		echo $this->Html->css('vendor/tagify/dist/tagify.css');
		?>
		<!-- End Page level css : manage-client  -->
		<?php
	}
	
	
	/* *** Page level css :scheduled  *** */
	
	if($actionName == 'scheduled')
	{
		?>
		 <!-- Page level script :scheduled  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
	
		?>
		<!-- End Page level script :scheduled  -->
		<?php
	}

	/* *** Page level css :chatbot  *** */
	
	if($actionName == 'chatbot')
	{
		?>
		 <!-- Page level script :chatbot  -->
		<?php
		echo $this->Html->css('vendor/nouislider/nouislider.min.css');
		echo $this->Html->css('vendor/clockpicker/css/bootstrap-clockpicker.min.css');
		echo $this->Html->css('vendor/jquery-asColorPicker/css/asColorPicker.min.css');
		echo $this->Html->css('vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
	
		?>
		<!-- End Page level script :chatbot  -->
		<?php
	}

	/* *** Page level css :fine-tune-models  *** */
	
	if($actionName == 'fineTuneModels')
	{
		?>
		 <!-- Page level script :fine-tune-models  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('vendor/jquery-asColorPicker/css/asColorPicker.min.css');
		echo $this->Html->css('vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
	
		?>
		<!-- End Page level script :fine-tune-models  -->
		<?php
	}

	/* *** Page level css :prompt  *** */
	
	if($actionName == 'prompt')
	{
		?>
		 <!-- Page level script :prompt  -->
		<?php
		echo $this->Html->css('vendor/nouislider/nouislider.min.css');
	
		?>
		<!-- End Page level script :prompt  -->
		<?php
	}

	/* *** Page level css :setting  *** */
	
	if($actionName == 'setting')
	{
		?>
		 <!-- Page level script :setting  -->
		<?php
		echo $this->Html->css('vendor/nouislider/nouislider.min.css');
		echo $this->Html->css('vendor/tagify/dist/tagify.css');
	
		?>
		<!-- End Page level script :setting  -->
		<?php
	}

	/* *** Page level css :content  *** */
	
	if($actionName == 'content')
	{
		?>
		 <!-- Page level script :content  -->
		<?php
		echo $this->Html->css('vendor/bootstrap-datepicker-master/css/bootstrap-datepicker.min.css');
		//echo $this->Html->css('vendor/tagify/dist/tagify.css');
	
		?>
		<!-- End Page level script :content  -->
		<?php
	}

	/* *** Page level css :content-add  *** */
	
	if($actionName == 'contentAdd')
	{
		?>
		 <!-- Page level script :content-add  -->
		<?php
		echo $this->Html->css('vendor/select2/css/select2.min.css');
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
	
		?>
		<!-- End Page level script :content-add  -->
		<?php
	}

	/* *** Page level css :menu  *** */
	
	if($actionName == 'menu')
	{
		?>
		 <!-- Page level script :menu  -->
		<?php
		echo $this->Html->css('vendor/nestable2/css/jquery.nestable.min.css');
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
	
		?>
		<!-- End Page level script :menu  -->
		<?php
	}

	/* *** Page level css :import  *** */
	
	if($actionName == 'import')
	{
		?>
		 <!-- Page level script :import  -->
		<?php
		echo $this->Html->css('vendor/dropzone/dist/dropzone.css');
	
		?>
		<!-- End Page level script :import  -->
		<?php
	}
	
	/* *** Page level css :post-details  *** */
	
	if($actionName == 'postDetails')
	{
		?>
		<!-- Page level css :post-details  -->
		<?php
		echo $this->Html->css('vendor/lightgallery/dist/css/lightgallery.css');
		echo $this->Html->css('vendor/lightgallery/dist/css/lg-thumbnail.css');
		echo $this->Html->css('vendor/lightgallery/dist/css/lg-zoom.css');
		?>
		<!-- Page level css :post-details  -->
		<?php
	}
	
	/* *** Page level css :user-list-datatable  *** */
	
	if($actionName == 'userListDatatable')
	{
		?>
		<!-- Page level css :user-list-datatable  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		?>
		<!-- End Page level css : user-list-datatable  -->
		<?php
	}
	
	/* *** Page level css :contact-list  *** */
	
	if($actionName == 'contactList')
	{
		?>
		<!-- Page level css :contact-list  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		?>
		<!-- End Page level css : contact-list  -->
		<?php
	}

	/* *** Page level css :email-compose  *** */

	if($actionName == 'emailCompose')
	{
		?>
		<!-- Page level css :email-compose  -->
		<?php
		echo $this->Html->css('vendor/dropzone/dist/dropzone.css');
		?>
		<!-- End Page level css : email-compose  -->
		<?php
	}
	
	/* *** Page level css :app-calender  *** */
	
	if($actionName == 'appCalender')
	{
		?>
		<!-- Page level css :app-calender  -->
		<?php
		echo $this->Html->css('vendor/fullcalendar/css/main.min.css');
		?>
		<!-- End Page level css : app-calender  -->
		<?php
	}
	
	/* *** Page level css :ecom-product-list  *** */
	
	if($actionName == 'ecomProductList')
	{
		?>
		<!-- Page level css :ecom-product-list  -->
		<?php
		echo $this->Html->css('vendor/star-rating/star-rating-svg.css');
		?>
		<!-- End Page level css : ecom-product-list  -->
		<?php
	}
	
	/* *** Page level css :ecom-product-detail  *** */
	
	if($actionName == 'ecomProductDetail')
	{
		?>
		<!-- Page level css :ecom-product-detail  -->
		<?php
		echo $this->Html->css('vendor/star-rating/star-rating-svg.css');
		echo $this->Html->css('vendor/owl-carousel/owl.carousel.css');
		?>
		<!-- End Page level css : ecom-product-detail  -->
		<?php
	}
	
	/* *** Page level css :chart-chartist  *** */
	
	if($actionName == 'chartChartist')
	{
		?>
		<!-- Page level css :chart-chartist  -->
		<?php
		echo $this->Html->css('vendor/chartist/css/chartist.min.css');
		?>
		<!-- End Page level css : chart-chartist  -->
		<?php
	}
	
	/* *** Page level css :uc-select2  *** */
	
	
	if($actionName == 'ucSelect2')
	{
		?>
		<!-- Page level css :uc-select2  -->
		<?php
		echo $this->Html->css('vendor/select2/css/select2.min.css');
		?>
		<!-- End Page level css : uc-select2  -->
		<?php
	}
	
	/* *** Page level css :uc-nestable  *** */
	
	if($actionName == 'ucNestable')
	{
		?>
		<!-- Page level css :uc-nestable  -->
		<?php
		echo $this->Html->css('vendor/nestable2/css/jquery.nestable.min.css');
		?>
		<!-- End Page level css : uc-nestable  -->
		<?php
	}
	
	/* *** Page level css :uc-toastr  *** */
	if($actionName == 'ucToastr')
	{
		?>
		<!-- Page level css :uc-toastr  -->
		<?php
		echo $this->Html->css('vendor/toastr/css/toastr.min.css');
		?>
		<!-- End Page level css : uc-toastr  -->
		<?php
	}
	
	/* *** Page level css :uc-noui-slider  *** */
	
	if($actionName == 'ucNouiSlider')
	{
		?>
		<!-- Page level css :uc-noui-slider  -->
		<?php
		echo $this->Html->css('vendor/nouislider/nouislider.min.css');
		?>
		<!-- End Page level css : uc-noui-slider  -->
		<?php
	}
	
	/* *** Page level css :uc-sweetalert  *** */
	
	if($actionName == 'ucSweetalert')
	{
		?>
		<!-- Page level css :uc-sweetalert  -->
		<?php
		echo $this->Html->css('vendor/sweetalert2/dist/sweetalert2.min.css');
		?>
		<!-- End Page level css : uc-sweetalert  -->
		<?php
	}
	
	/* *** Page level css :uc-toastr  *** */
	
	if($actionName == 'ucToast')
	{
		?>
		<!-- Page level css :uc-toastr  -->
		<?php
		echo $this->Html->css('vendor/toastr/css/toastr.min.css');
		?>
		<!-- End Page level css : uc-toastr  -->
		<?php
	}
	
	/* *** Page level css :map-jqvmap  *** */
	
	if($actionName == 'mapJqvmap')
	{
		?>
		<!-- Page level css :map-jqvmap  -->
		<?php
		echo $this->Html->css('vendor/jqvmap/css/jqvmap.min.css');
		?>
		<!-- End Page level css : map-jqvmap  -->
		<?php
	}
	
	/* *** Page level css :uc-lightgallery  *** */
	
	if($actionName == 'ucLightgallery')
	{
		?>
		<!-- Page level css :uc-lightgallery  -->
		<?php
		echo $this->Html->css('vendor/lightgallery/dist/css/lightgallery.css');
		echo $this->Html->css('vendor/lightgallery/dist/css/lg-thumbnail.css');
		echo $this->Html->css('vendor/lightgallery/dist/css/lg-zoom.css');
		?>
		<!-- End Page level css : uc-lightgallery  -->
		<?php
	}
	
	/* *** Page level css :widget-basic  *** */
	
	if($actionName == 'widgetBasic')
	{
		?>
		<!-- Page level css :widget-basic  -->
		<?php
		echo $this->Html->css('vendor/chartist/css/chartist.min.css');
		?>
		<!-- End Page level css : widget-basic  -->
		<?php
	}
	
	/* *** Page level css :form-wizard  *** */
	
	if($actionName == 'formWizard')
	{
		?>
		<!-- Page level css :form-wizard  -->
		<?php
		echo $this->Html->css('vendor/jquery-smartwizard/dist/css/smart_wizard.min.css');
		?>
		<!-- End Page level css : form-wizard  -->
		<?php
	}
	
	/* *** Page level css :form-pickers  *** */
	
	if($actionName == 'formPickers')
	{
		?>
		<!-- Page level css :form-pickers  -->
		<?php
		echo $this->Html->css('vendor/bootstrap-daterangepicker/daterangepicker.css');
		echo $this->Html->css('vendor/clockpicker/css/bootstrap-clockpicker.min.css');
		echo $this->Html->css('vendor/jquery-asColorPicker/css/asColorPicker.min.css');
		echo $this->Html->css('vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
		echo $this->Html->css('vendor/pickadate/themes/default.css'); 
		echo $this->Html->css('vendor/pickadate/themes/default.date.css'); 
		echo $this->Html->css('https://fonts.googleapis.com/icon?family=Material+Icons'); 
		?>
		<!-- End Page level css : form-pickers  -->
		<?php
	}
	
	/* *** Page level css :table-datatable-basic  *** */
	
	if($actionName == 'tableDatatableBasic')
	{
		?>
		<!-- Page level css :table-datatable-basic  -->
		<?php
		echo $this->Html->css('vendor/datatables/css/jquery.dataTables.min.css');
		echo $this->Html->css('vendor/datatables/css/responsive.bootstrap.min.css');
		?>
		<!-- End Page level css : table-datatable-basic  -->
		<?php
	}

	echo $this->Html->css('css/style.css', ['rel' => 'stylesheet']);
	?>
	
    
</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <?= $this->element('preloader') ?>
    <!--*******************
        Preloader end
    ********************-->
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

    <?= $this->element('nav-header') ?>
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Chat box start
        ***********************************-->
        <?= $this->element('chatbox') ?>
		<!--**********************************
            Chat box End
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        <?= $this->element('header') ?>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?= $this->element('sidebar') ?>
        <!--**********************************
            Sidebar end
        ***********************************-->
        
        <!--**********************************
            Content body start
        ***********************************-->
		<?php
			
			$pageBodyClasses = array(
				'uiBadge' => 'badge-demo',
				'uiButton' => 'btn-page',
				'emailCompose' => 'inbox-height',
				'emailInbox' => 'inbox-height',
				'chat' => 'chat-height',
			);
			if (array_key_exists($actionName, $pageBodyClasses)) {
				$bodyClass = $pageBodyClasses[$actionName];
			} else {
				$bodyClass = '';
			}
			?>
       	<div class="content-body default-height <?php echo $bodyClass; ?>">
			<?= $this->fetch('content') ?>
		</div>    
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->

			
        <?= $this->element('footer'); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
	<script>
    	var csrfToken = '<?= $this->request->getAttribute('csrfToken') ?>'; 
    </script>
			<script>var asset_base_url = '<?= $this->Url->webroot('/'); ?>';</script>
    <?php
	/* *** Page level script : Top  *** */
	 echo $this->Html->script('vendor/global/global.min.js');
	 echo $this->Html->script('vendor/bootstrap-select/dist/js/bootstrap-select.min.js');
	 echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
	 
	/* *** End Page level script : Top   *** */
	
	/* ***  Page level css : Dashboard  *** */
	
	if($actionName == 'index')
	{
		?>
		<!-- Page level css : Dashboard 1 -->
		<?php
		echo $this->Html->script('vendor/chart.js/Chart.bundle.min.js'); 
		echo $this->Html->script('vendor/peity/jquery.peity.min.js'); 
		echo $this->Html->script('vendor/apexchart/apexchart.js'); 
		echo $this->Html->script('js/dashboard/dashboard-1.js'); 
		?>
		<!-- Page level Js : Dashboard  -->
		<?php
	}	
	
	/* ***  Page level css : Dashboard 2  *** */
	if($actionName == 'index2')
	{
		?>
		<!-- Page level css : Dashboard 2 -->
		<?php
		echo $this->Html->script('vendor/chart.js/Chart.bundle.min.js');
		echo $this->Html->script('vendor/peity/jquery.peity.min.js');
		echo $this->Html->script('vendor/apexchart/apexchart.js');
		echo $this->Html->script('js/dashboard/dashboard-1.js');
		?>
		<!-- Page level Js : Dashboard 2  -->
		<?php
	}	
	
	/* ***  Page level Js : order-list  *** */
	
	if($actionName == 'orders'){
		
		?>
		<!-- Page level script : order-list  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js'); 

		
		?>
		<!-- End Page level script : order-list  -->
		<?php
	}
	/* ***  Page level Js : general-customers  *** */
	
	if($actionName == 'generalCustomers'){
		
		?>
		<!-- Page level script :general-customers  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js'); 

		
		?>
		<!-- End Page level script :general-customers  -->
		<?php
	}
	
	/* *** Page level JS : Event  *** */
	
	if($actionName == 'event')
	{
		?>
		<!-- Page level script : Event  -->
		<?php
		echo $this->Html->script('vendor/chartjs/chart.bundle.min.js');
		echo $this->Html->script('vendor/peity/jquery.peity.min.js');
		echo $this->Html->script('vendor/apexchart/apexchart.js');
		echo $this->Html->script('js/dashboard/event.js');
		echo $this->Html->script('vendor/owl-carousel/owl.carousel.js');
		?>
		<!-- End Page level script : Event  -->
		<?php
	}
	
	/* *** Page level JS : Customer-list *** */
	
	if($actionName == 'customer')
	{
		?>
		<!-- Page level script : Customer-list  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
		?>
		<!-- End Page level script : Customer-list  -->
		<?php
	}
	
	/* ***  Page level Js :analytics  *** */
	
	if($actionName == 'analytics')
	{
		?>
		<!-- Page level script : analytics  -->
		<?php
		echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
		echo $this->Html->script('vendor/chart.js/Chart.bundle.min.js');
		echo $this->Html->script('vendor/waypoints/jquery.waypoints.min.js');
		echo $this->Html->script('vendor/jquery.counterup/jquery.counterup.min.js');
		echo $this->Html->script('vendor/peity/jquery.peity.min.js');
		echo $this->Html->script('vendor/apexchart/apexchart.js');
		echo $this->Html->script('js/dashboard/analytics.js');
		?>
		<!-- End Page level script : analytics  -->
		<?php
	}	
	
	/* *** Page level js :market-capital  *** */
	
	if($actionName == 'taskSummary')
	{
		?>
		<!-- Page level script : task-summary -->
		<?php
		echo $this->Html->script('vendor/draggable/draggable.js');
		echo $this->Html->script('vendor/tagify/dist/tagify.js');
		?>
		<!-- End Page level script : task-summary  -->
		<?php
	}

	/* *** Page level js :reviews  *** */
	
	if($actionName == 'reviews')
	{
		?>
		<!-- Page level script : reviews -->
		<?php
		echo $this->Html->script('vendor/owl-carousel/owl.carousel.js');
		?>
		<!-- End Page level script : reviews  -->
		<?php
	}

	/* *** Page level js :project  *** */
	
	if($actionName == 'project')
	{
		?>
		<!-- Page level script : project -->
		<?php
		
		echo $this->Html->script('vendor/tagify/dist/tagify.js'); 
		?>
		<!-- End Page level script : project  -->
		<?php
	}

	/* *** Page level js :Reports  *** */

	if($actionName == 'reports')
	{
		?>
		<!-- Page level script : Reports -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('vendor/datatables/js/dataTables.buttons.min.js');
		echo $this->Html->script('vendor/datatables/js/buttons.html5.min.js');
		echo $this->Html->script('vendor/datatables/js/jszip.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		echo $this->Html->script('vendor/tagify/dist/tagify.js');
		?>
		<!-- End Page level script : Reports  -->
		<?php
	}

	
	
	/* *** Page level js :post-details  *** */
	
	if($actionName == 'postDetails')
	{
		?>
		<!-- Page level script : post-details  -->
		<?php
		echo $this->Html->script('vendor/lightgallery/dist/lightgallery.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/thumbnail/lg-thumbnail.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/zoom/lg-zoom.min.js');
		?>
		<!-- End Page level script :post-details  -->
		<?php
	}
	
	/* *** Page level js :Chat  *** */
	
	if($actionName == 'pageChat')
	{
		?>
		  <!-- Page level script : chat  -->
		<?php
		echo $this->Html->script('js/dashboard/chat.js');
		?>
		<!-- End Page level script :chat  -->
		<?php
	}
	
	/* *** Page level js :user-list-datatable  *** */
	
	if($actionName == 'userListDatatable')
	{
		?>
		 <!-- Page level script : user-list-datatable  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		?>
		<!-- End Page level script :user-list-datatable  -->
		<?php
	}
	
	/* *** Page level js :contact-list  *** */
	
	if($actionName == 'contactList')
	{
		?>
		<!-- Page level script : contact-list  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		?>
		<!-- End Page level script :contact-list  -->
		<?php
	}
	
	/* *** Page level js :email-compose  *** */

	if($actionName == 'emailCompose')
	{
		?>
		<!-- Page level script : email-compose  -->
		<?php
		echo $this->Html->script('vendor/dropzone/dist/dropzone.js');
		?>
		<!-- End Page level script :email-compose  -->
		<?php
	}
	
	/* *** Page level js :app-calender  *** */
	
	if($actionName == 'appCalender')
	{
		?>
		 <!-- Page level script :app-calender  -->
		<?php
		echo $this->Html->script('vendor/moment/moment.min.js'); 
		echo $this->Html->script('vendor/fullcalendar/js/main.min.js'); 
		echo $this->Html->script('js/plugins-init/fullcalendar-init.js'); 
		?>
		<!-- End Page level script :app-calender  -->
		<?php
	}
	
	/* *** Page level js :ecom-product-list  *** */
	
	if($actionName == 'ecomProductList')
	{
		?>
		<!-- Page level script :ecom-product-list  -->
		<?php
		echo $this->Html->script('vendor/star-rating/jquery.star-rating-svg.js');
		?>
		<!-- End Page level script :ecom-product-list  -->
		<?php
	}
	
	/* *** Page level js :ecom-product-detail  *** */
	
	if($actionName == 'ecomProductDetail')
	{
		?>
		 <!-- Page level script :ecom-product-detail  -->
		<?php
		echo $this->Html->script('vendor/star-rating/jquery.star-rating-svg.js');
		echo $this->Html->script('vendor/owl-carousel/owl.carousel.js');
		?>
		<!-- End Page level script :ecom-product-detail  -->
		<?php
	}

	/* *** Page level js :scheduled  *** */
	
	if($actionName == 'scheduled')
	{
		?>
		 <!-- Page level script :scheduled  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		echo $this->Html->script('vendor/tagify/dist/tagify.js'); 
		?>
		<!-- End Page level script :scheduled  -->
		<?php
	}

	/* *** Page level js :chatbot  *** */
	
	if($actionName == 'chatbot')
	{
		?>
		 <!-- Page level script :chatbot  -->
		<?php
		echo $this->Html->script('vendor/nouislider/nouislider.min.js');
		echo $this->Html->script('vendor/wnumb/wNumb.js');
		echo $this->Html->script('vendor/jquery-asColor/jquery-asColor.min.js');
		echo $this->Html->script('vendor/jquery-asGradient/jquery-asGradient.min.js');
		echo $this->Html->script('vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js');
		echo $this->Html->script('js/plugins-init/jquery-asColorPicker.init.js');
		?>
		<!-- End Page level script :chatbot  -->
		<?php
	}

	/* *** Page level js :fine-tune-models  *** */
	
	if($actionName == 'fineTuneModels')
	{
		?>
		 <!-- Page level script :fine-tune-models  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('vendor/datatables/js/dataTables.buttons.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
	
		?>
		<!-- End Page level script :fine-tune-models  -->
		<?php
	}

	/* *** Page level js :prompt  *** */
	
	if($actionName == 'prompt')
	{
		?>
		 <!-- Page level script :prompt  -->
		<?php
		echo $this->Html->script('vendor/nouislider/nouislider.min.js');
		echo $this->Html->script('vendor/wnumb/wNumb.js');
	
		?>
		<!-- End Page level script :prompt  -->
		<?php
	}

	/* *** Page level js :setting  *** */
	
	if($actionName == 'setting')
	{
		?>
		 <!-- Page level script :setting  -->
		<?php
		echo $this->Html->script('vendor/nouislider/nouislider.min.js');
		echo $this->Html->script('vendor/wnumb/wNumb.js');
		echo $this->Html->script('vendor/tagify/dist/tagify.js');
	
		?>
		<!-- End Page level script :setting  -->
		<?php
	}

	/* *** Page level js :content  *** */
	
	if($actionName == 'content')
	{
		?>
		 <!-- Page level script :content  -->
		<?php
		echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
		echo $this->Html->script('js/dashboard/cms.js');
		
	
		?>
		<!-- End Page level script :content  -->
		<?php
	}

	/* *** Page level js :content-add  *** */
	
	if($actionName == 'contentAdd')
	{
		?>
		 <!-- Page level script :content-add  -->
		<?php
		echo $this->Html->script('vendor/select2/js/select2.full.min.js');
		echo $this->Html->script('js/plugins-init/select2-init.js');
		echo $this->Html->script('vendor/ckeditor/ckeditor.js');
		echo $this->Html->script('js/dashboard/cms.js');
		
	
		?>
		<!-- End Page level script :content-add  -->
		<?php
	}

	/* *** Page level js :menu  *** */
	
	if($actionName == 'menu')
	{
		?>
		 <!-- Page level script :menu  -->
		<?php
		
		echo $this->Html->script('vendor/nestable2/js/jquery.nestable.min.js');
		echo $this->Html->script('js/plugins-init/nestable-init.js');
		echo $this->Html->script('js/dashboard/cms.js');
	
		?>
		<!-- End Page level script :menu  -->
		<?php
	}

	/* *** Page level js :add-email  *** */
	
	if($actionName == 'addEmail')
	{
		?>
		 <!-- Page level script :add-email  -->
		<?php
		
		echo $this->Html->script('vendor/ckeditor/ckeditor.js');
		echo $this->Html->script('vendor/select2/js/select2.full.min.js');
		echo $this->Html->script('js/plugins-init/select2-init.js');
		echo $this->Html->script('js/plugins-init/select2-init.js');
		echo $this->Html->script('js/dashboard/cms.js');
	
		?>
		<!-- End Page level script :add-email  -->
		<?php
	}

	/* *** Page level js :email-template  *** */
	
	if($actionName == 'emailTemplate')
	{
		?>
		 <!-- Page level script :email-template  -->
		<?php
		
		echo $this->Html->script('js/dashboard/cms.js');
	
		?>
		<!-- End Page level script :email-template  -->
		<?php
	}

	/* *** Page level js :blog  *** */
	
	if($actionName == 'blog')
	{
		?>
		 <!-- Page level script :blog  -->
		<?php
		echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
		echo $this->Html->script('js/dashboard/cms.js');
	
		?>
		<!-- End Page level script :blog  -->
		<?php
	}

	/* *** Page level js :blog-category  *** */
	
	if($actionName == 'blogCategory')
	{
		?>
		 <!-- Page level script :blog-category  -->
		<?php
		echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
		echo $this->Html->script('js/dashboard/cms.js');
		
		
	
		?>
		<!-- End Page level script :blog-category  -->
		<?php
	}

	/* *** Page level js :add-blog  *** */
	
	if($actionName == 'addBlog')
	{
		?>
		 <!-- Page level script :add-blog  -->
		<?php
		
		echo $this->Html->script('vendor/ckeditor/ckeditor.js');
		echo $this->Html->script('vendor/select2/js/select2.full.min.js');
		echo $this->Html->script('js/plugins-init/select2-init.js');
		echo $this->Html->script('js/dashboard/cms.js');
		
	
		?>
		<!-- End Page level script :add-blog  -->
		<?php
	}

	/* *** Page level js :import  *** */
	
	if($actionName == 'import')
	{
		?>
		 <!-- Page level script :import  -->
		<?php
		echo $this->Html->script('vendor/dropzone/dist/dropzone.js');
	
		?>
		<!-- End Page level script :import  -->
		<?php
	}
	
	/* *** Page level js :chart-flot  *** */
	
	if($actionName == 'chartFlot')
	{
		?>
		 <!-- Page level script :chart-flot  -->
		<?php
		
		echo $this->Html->script('vendor/flot/jquery.flot.js'); 
		echo $this->Html->script('vendor/flot/jquery.flot.pie.js'); 
		echo $this->Html->script('vendor/flot/jquery.flot.resize.js'); 
		echo $this->Html->script('vendor/flot-spline/jquery.flot.spline.min.js'); 
		echo $this->Html->script('js/plugins-init/flot-init.js'); 
		?>
		<!-- End Page level script :chart-flot  -->
		<?php
	}
	
	/* *** Page level js :chart-morris  *** */
	
	if($actionName == 'chartMorris')
	{
		?>
		 <!-- Page level script :chart-morris  -->
		<?php
		
		echo $this->Html->script('vendor/raphael/raphael.min.js');
		echo $this->Html->script('vendor/morris/morris.min.js');
		echo $this->Html->script('js/plugins-init/morris-init.js');
		?>
		<!-- End Page level script :chart-morris  -->
		<?php
	}
	
	/* *** Page level js :chart-chartjs  *** */
	
	if($actionName == 'chartChartjs')
	{
		?>
		 <!-- Page level script :chart-chartjs  -->
		<?php
		echo $this->Html->script('vendor/chart.js/Chart.bundle.min.js'); 
		echo $this->Html->script('vendor/apexchart/apexchart.js'); 
		echo $this->Html->script('vendor/apexchart/apexchart.js'); 
		echo $this->Html->script('js/plugins-init/chartjs-init.js'); 
		?>
		<!-- End Page level script :chart-chartjs  -->
		<?php
	}
	
	/* *** Page level js :chart-chartist  *** */
	
	if($actionName == 'chartChartist')
	{
		?>
		 <!-- Page level script :chart-chartist  -->
		<?php
		echo $this->Html->script('vendor/chartist/js/chartist.min.js'); 
		echo $this->Html->script('vendor/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js'); 
		echo $this->Html->script('js/plugins-init/chartist-init.js'); 
		?>
		<!-- End Page level script :chart-chartist  -->
		<?php
	}
	
	/* *** Page level css :chart-sparkline  *** */
	
	if($actionName == 'chartSparkline')
	{
		?>
		 <!-- Page level script :chart-sparkline  -->
		<?php
		
		echo $this->Html->script('vendor/jquery-sparkline/jquery.sparkline.min.js'); 
		echo $this->Html->script('js/plugins-init/sparkline-init.js');
		
		?>
		<!-- End Page level script :chart-sparkline  -->
		<?php
	}
	
	/* *** Page level js :chart-peity  *** */
	
	if($actionName == 'chartPeity')
	{
		?>
		 <!-- Page level script :chart-peity  -->
		<?php
		echo $this->Html->script('vendor/peity/jquery.peity.min.js'); 
		echo $this->Html->script('js/plugins-init/piety-init.js'); 
		?>
		<!-- End Page level script :chart-peity  -->
		<?php
	}

	/* *** Page level js :ui-accordion  *** */
	
	if($actionName == 'uiAccordion')
	{
		?>
		 <!-- Page level script :ui-accordion  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-accordion  -->
		<?php
	}

	/* *** Page level js :ui-alert  *** */
	
	if($actionName == 'uiAlert')
	{
		?>
		 <!-- Page level script :ui-alart  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-alert  -->
		<?php
	}

	/* *** Page level js :ui-badge  *** */
	
	if($actionName == 'uiBadge')
	{
		?>
		 <!-- Page level script :ui-badge  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-badge  -->
		<?php
	}

	/* *** Page level js :ui-button  *** */
	
	if($actionName == 'uiButton')
	{
		?>
		 <!-- Page level script :ui-button  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-button  -->
		<?php
	}

	/* *** Page level js :ui-button-group  *** */
	
	if($actionName == 'uiButtonGroup')
	{
		?>
		 <!-- Page level script :button-group  -->
		<?php
		 
		?>
		<!-- End Page level script :button-group  -->
		<?php
	}

	/* *** Page level js :ui-card  *** */
	
	if($actionName == 'uiCard')
	{
		?>
		 <!-- Page level script :ui-card  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-card  -->
		<?php
	}

	/* *** Page level js :ui-dropdown  *** */
	
	if($actionName == 'uiDropdown')
	{
		?>
		 <!-- Page level script :ui-dropdown  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-dropdown  -->
		<?php
	}

	/* *** Page level js :ui-progressbar  *** */
	
	if($actionName == 'uiProgressbar')
	{
		?>
		 <!-- Page level script :ui-progressbar  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-progressbar  -->
		<?php
	}

	/* *** Page level js :ui-pagination  *** */
	
	if($actionName == 'uiPagination')
	{
		?>
		 <!-- Page level script :ui-pagination  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-pagination  -->
		<?php
	}

	/* *** Page level js :ui-tab  *** */
	
	if($actionName == 'uiTab')
	{
		?>
		 <!-- Page level script :ui-tab  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-tab  -->
		<?php
	}


	/* *** Page level js :ui-carousel  *** */
	
	if($actionName == 'uiCarousel')
	{
		?>
		 <!-- Page level script :ui-carousel  -->
		<?php
		 
		?>
		<!-- End Page level script :ui-carousel  -->
		<?php
	}
	
	
	/* *** Page level js :uc-select2  *** */
	
	if($actionName == 'ucSelect2')
	{
		?>
		<!-- Page level js :uc-select2  -->
		<?php
		echo $this->Html->script('vendor/select2/js/select2.full.min.js');
		echo $this->Html->script('js/plugins-init/select2-init.js');
		?>
		<!-- End Page level js : uc-select2  -->
		<?php
	}
	
	/* *** Page level js :uc-nestable  *** */
	
	if($actionName == 'ucNestable')
	{
		?>
		<!-- Page level js :uc-nestable  -->
		<?php
		echo $this->Html->script('vendor/nestable2/js/jquery.nestable.min.js');
		echo $this->Html->script('js/plugins-init/nestable-init.js');
		?>
		<!-- End Page level js : uc-nestable  -->
		<?php
	}
	
	/* *** Page level js :uc-noui-slider  *** */
	
	if($actionName == 'ucNouiSlider')
	{
		?>
		<!-- Page level js :uc-noui-slider  -->
		<?php
		echo $this->Html->script('vendor/nouislider/nouislider.min.js');
		echo $this->Html->script('vendor/wnumb/wNumb.js');
		echo $this->Html->script('js/plugins-init/nouislider-init.js');
		?>
		<!-- End Page level js : uc-noui-slider  -->
		<?php
	}
	
	/* *** Page level js :uc-sweetalert  *** */
	
	if($actionName == 'ucSweetalert')
	{
		?>
		<!-- Page level js :uc-sweetalert  -->
		<?php
		echo $this->Html->script('vendor/sweetalert2/dist/sweetalert2.min.js');
		echo $this->Html->script('js/plugins-init/sweetalert.init.js');
		?>
		<!-- End Page level js : uc-sweetalert  -->
		<?php
	}
	
	/* *** Page level js :uc-toastr  *** */
	
	if($actionName == 'ucToastr')
	{
		?>
		<!-- Page level js :uc-toastr  -->
		<?php
		echo $this->Html->script('vendor/toastr/js/toastr.min.js');
		echo $this->Html->script('js/plugins-init/toastr-init.js');
		?>
		<!-- End Page level js : uc-toastr  -->
		<?php
	}
	
	/* *** Page level js :map-jqvmap  *** */
	
	if($actionName == 'mapJqvmap')
	{
		?>
		<!-- Page level js :map-jqvmap  -->
		<?php
		echo $this->Html->script('vendor/jqvmap/js/jquery.vmap.min.js');
		echo $this->Html->script('vendor/jqvmap/js/jquery.vmap.world.js');
		echo $this->Html->script('vendor/jqvmap/js/jquery.vmap.usa.js');
		echo $this->Html->script('js/plugins-init/jqvmap-init.js');
		?>
		<!-- End Page level js : map-jqvmap  -->
		<?php
	}
	/* *** Page level js :uc-lightgallery  *** */
	
	if($actionName == 'ucLightgallery')
	{
		?>
		<!-- Page level js :uc-lightgallery  -->
		<?php
		echo $this->Html->script('vendor/lightgallery/dist/lightgallery.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/thumbnail/lg-thumbnail.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/thumbnail/lg-thumbnail.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/zoom/lg-zoom.min.js');
		?>
		<!-- End Page level js : uc-lightgallery  -->
		<?php
	}
	
	/* *** Page level js :widget-basic  *** */
	
	if($actionName == 'widgetBasic')
	{
		?>
		<!-- Page level js :widget-basic  -->
		<?php
		echo $this->Html->script('vendor/chart.js/Chart.bundle.min.js'); 
		echo $this->Html->script('vendor/apexchart/apexchart.js'); 
		echo $this->Html->script('vendor/chartist/js/chartist.min.js'); 
		echo $this->Html->script('vendor/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js'); 
		echo $this->Html->script('vendor/flot/jquery.flot.js'); 
		echo $this->Html->script('vendor/flot/jquery.flot.pie.js'); 
		echo $this->Html->script('vendor/flot/jquery.flot.resize.js'); 
		echo $this->Html->script('vendor/flot-spline/jquery.flot.spline.min.js'); 
		echo $this->Html->script('vendor/jquery-sparkline/jquery.sparkline.min.js'); 
		echo $this->Html->script('js/plugins-init/sparkline-init.js'); 
		echo $this->Html->script('vendor/peity/jquery.peity.min.js'); 
		echo $this->Html->script('js/plugins-init/piety-init.js');
		echo $this->Html->script('js/plugins-init/widgets-script-init.js'); 
		?>
		<!-- End Page level js : widget-basic  -->
		<?php
	}
	
	/* *** Page level js :app-profile  *** */
	
	if($actionName == 'appProfile')
	{
		?>
		<!-- Page level js :app-profile  -->
		<?php
		echo $this->Html->script('vendor/lightgallery/dist/lightgallery.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/thumbnail/lg-thumbnail.min.js');
		echo $this->Html->script('vendor/lightgallery/dist/plugins/zoom/lg-zoom.min.js');
		?>
		<!-- End Page level js : app-profile  -->
		<?php
	}
	/* *** Page level js :customer  *** */
	
	if($actionName == 'customer')
	{
		?>
		<!-- Page level js :customer  -->
		<?php
		
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		echo $this->Html->script('vendor/bootstrap-datepicker-master/js/bootstrap-datepicker.min.js');
		
		?>
		<!-- End Page level js : customer  -->
		<?php
	}

	/* *** Page level js :customer-profile  *** */
	
	if($actionName == 'customerProfile')
	{
		?>
		<!-- Page level js :customer-profile  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('vendor/datatables/js/dataTables.buttons.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		echo $this->Html->script('vendor/tagify/dist/tagify.js');
		?>
		<!-- End Page level js : customer-profile  -->
		<?php
	}

	/* *** Page level js :contacts  *** */
	if($actionName == 'contacts')
	{
		?>
		<!-- Page level js :contacts  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('vendor/datatables/js/dataTables.buttons.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		echo $this->Html->script('vendor/tagify/dist/tagify.js');
		?>
		<!-- End Page level js : contacts  -->
		<?php
	}
	
	/* *** Page level js :form-wizard  *** */
	
	if($actionName == 'formWizard')
	{
		?>
		<!-- Page level js :form-wizard  -->
		<?php
		echo $this->Html->script('vendor/jquery-steps/build/jquery.steps.min.js'); 
		echo $this->Html->script('vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js'); 
		?>
		<!-- End Page level js : form-wizard  -->
		<?php
	}
	
	/* *** Page level js :form-ckeditor  *** */
	
	if($actionName == 'formEditor')
	{
		?>
		<!-- Page level css :form-ckeditor-ckeditor  -->
		<?php
		echo $this->Html->script('vendor/ckeditor/ckeditor.js');
		echo $this->Html->script('vendor/summernote/js/summernote.min.js');
		echo $this->Html->script('js/plugins-init/summernote-init.js');
		?>
		<!-- End Page level js : form-editor-ckeditor  -->
		<?php
	}

	/* *** Page level js :-summernote  *** */
	
	if($actionName == 'formEditorSummernote')
	{
		?>
		<!-- Page level css :summernote  -->
		<?php
		echo $this->Html->script('vendor/summernote/js/summernote.min.js');
		echo $this->Html->script('js/plugins-init/summernote-init.js');
		?>
		<!-- End Page level js : summernote  -->
		<?php
	}
	
	/* *** Page level js :form-pickers  *** */
	
	if($actionName == 'formPickers')
	{
		?>
		<!-- Page level css :form-pickers  -->
		<?php
		echo $this->Html->script('vendor/moment/moment.min.js'); 
		echo $this->Html->script('vendor/bootstrap-daterangepicker/daterangepicker.js'); 
		echo $this->Html->script('vendor/clockpicker/js/bootstrap-clockpicker.min.js'); 
		echo $this->Html->script('vendor/jquery-asColor/jquery-asColor.min.js');
		echo $this->Html->script('vendor/jquery-asGradient/jquery-asGradient.min.js'); 
		echo $this->Html->script('vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js');
		echo $this->Html->script('vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js');
		echo $this->Html->script('vendor/pickadate/picker.js');
		echo $this->Html->script('vendor/pickadate/picker.time.js'); 
		echo $this->Html->script('vendor/pickadate/picker.date.js'); 
		echo $this->Html->script('js/plugins-init/bs-daterange-picker-init.js'); 
		echo $this->Html->script('js/plugins-init/clock-picker-init.js'); 
		echo $this->Html->script('js/plugins-init/jquery-asColorPicker.init.js'); 
		echo $this->Html->script('js/plugins-init/material-date-picker-init.js'); 
		echo $this->Html->script('js/plugins-init/pickadate-init.js'); 
		?>
		<!-- End Page level js : form-pickers  -->
		<?php
	}

	/* *** Page level css :table-datatable-basic  *** */
	
	if($actionName == 'tableDatatableBasic')
	{
		?>
		<!-- Page level css :table-datatable-basic  -->
		<?php
		echo $this->Html->script('vendor/datatables/js/jquery.dataTables.min.js');
		echo $this->Html->script('vendor/datatables/js/dataTables.responsive.min.js');
		echo $this->Html->script('js/plugins-init/datatables.init.js');
		
		?>
		<!-- End Page level css : table-datatable-basic  -->
		<?php
	}

	/* *** Page level js :table-datatable-basic  *** */
	
	if($actionName == 'tableBootstrapBasic')
	{
		?>
		<!-- Page level js :table-bootstrap-basic  -->
		<?php
		
		?>
		<!-- End Page level js : table-bootstrap-basic  -->
		<?php
	}
	
	/* *** Page level script : Bottom  *** */
	
	echo $this->Html->script('js/custom.min.js'); 
	echo $this->Html->script('js/deznav-init.js'); 
	
	?>
   <?= $this->fetch('script') ?>
   
</body>
</html>