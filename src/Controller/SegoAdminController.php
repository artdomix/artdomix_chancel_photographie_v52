<?php
declare (strict_types = 1);

namespace App\Controller;

use Cake\Routing\Router;

/**
 * SegoAdmin Controller
 */
class SegoAdminController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('dashboard');
        $baseUrl = Router::url('/', true);
        $this->set('baseUrl', $baseUrl);
    }

    public function ajaxFavouriteItems()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->render('ajax_favourite_items');
    }

    public function ajaxSellingItems()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->render('ajax_selling_items');
    }

    public function ajaxUpcomingEvent()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->render('ajax_upcoming_event');
    }

    public function ajaxRecentList()
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->render('ajax_recent_list');
    }

    public function index()
    {
        $this->set('page_title', 'Dashboard');
        $this->set('description', 'Welcome to Sego Admin!');
    }

    public function index2()
    {
        $this->set('page_title', 'Dashboard');
        $this->set('description', '');
    }

    public function orders()
    {
        $this->set('page_title', 'Orders');
        $this->set('description', 'Here is your order list data');
    }

    public function orderId()
    {
        $this->set('page_title', 'Order ID #5552351');
        $this->set('description', '');
    }

    public function generalCustomers()
    {
        $this->set('page_title', 'General Customers');
        $this->set('description', 'Here is your general customers list data');
    }

    public function analytics()
    {
        $this->set('page_title', 'Analytics');
        $this->set('description', '');
    }

    public function reviews()
    {
        $this->set('page_title', 'Reviews');
        $this->set('description', '');
    }

    public function addBlog()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function addEmail()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function appCalender()
    {
        $this->set('page_title', 'Calender');
        $this->set('description', '');
    }

    public function appProfile()
    {
        $this->set('page_title', 'Profile');
        $this->set('description', '');
    }

    public function blog()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function blogCategory()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function chartChartist()
    {
        $this->set('page_title', 'Chartist');
        $this->set('description', '');
    }

    public function chartChartjs()
    {
        $this->set('page_title', 'ChartJS');
        $this->set('description', '');
    }

    public function chartFlot()
    {
        $this->set('page_title', 'Flot');
        $this->set('description', '');
    }

    public function chartMorris()
    {
        $this->set('page_title', 'Morris');
        $this->set('description', '');
    }

    public function chartPeity()
    {
        $this->set('page_title', 'Peity Chart');
        $this->set('description', '');
    }

    public function chartSparkline()
    {
        $this->set('page_title', 'Sparkline');
        $this->set('description', '');
    }

    public function content()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function uiMediaObject()
    {
        $this->set('page_title', 'Media Object');
        $this->set('description', '');
    }

    public function contentAdd()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function ecomCheckout()
    {
        $this->set('page_title', 'Checkout');
        $this->set('description', '');
    }

    public function ecomCustomers()
    {
        $this->set('page_title', 'Customers');
        $this->set('description', '');
    }

    public function ecomInvoice()
    {
        $this->set('page_title', 'Invoice');
        $this->set('description', '');
    }

    public function ecomProductDetail()
    {
        $this->set('page_title', 'Product Detail');
        $this->set('description', '');
    }

    public function ecomProductGrid()
    {
        $this->set('page_title', 'Product Grid');
        $this->set('description', '');
    }

    public function ecomProductList()
    {
        $this->set('page_title', 'Product List');
        $this->set('description', '');
    }

    public function ecomProductOrder()
    {
        $this->set('page_title', 'Product Order');
        $this->set('description', '');
    }

    public function emailCompose()
    {
        $this->set('page_title', 'Compose');
        $this->set('description', '');
    }

    public function emailInbox()
    {
        $this->set('page_title', 'Inbox');
        $this->set('description', '');
    }

    public function emailRead()
    {
        $this->set('page_title', 'Read');
        $this->set('description', '');
    }

    public function emailTemplate()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function emptyPage()
    {
        $this->set('page_title', 'Empty Page');
        $this->set('description', '');
    }

    public function feather()
    {
        $this->set('page_title', 'Feather Icons');
        $this->set('description', '');
    }

    public function flatIcons()
    {
        $this->set('page_title', 'Flat Icons');
        $this->set('description', '');
    }

    public function svgIcons()
    {
        $this->set('page_title', 'Svg Icons');
        $this->set('description', '');
    }

    public function formEditor()
    {
        $this->set('page_title', 'Form CkEditor');
        $this->set('description', '');
    }

    public function formElement()
    {
        $this->set('page_title', 'Form Element');
        $this->set('description', '');
    }

    public function formPickers()
    {
        $this->set('page_title', 'Pickers');
        $this->set('description', '');
    }

    public function formValidationJquery()
    {
        $this->set('page_title', 'Validation');
        $this->set('description', '');
    }

    public function formWizard()
    {
        $this->set('page_title', 'Form Wizard');
        $this->set('description', '');
    }

    public function mapJqvmap()
    {
        $this->set('page_title', 'JQV Map');
        $this->set('description', '');
    }

    public function menu()
    {
        $this->set('page_title', 'CMS');
        $this->set('description', '');
    }

    public function pageError400()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageError403()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageError404()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageError500()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageError503()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageForgotPassword()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageLockScreen()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageLogin()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function pageRegister()
    {
        $this->set('page_title', '');
        $this->set('description', '');
        $this->viewBuilder()->setLayout('login');
    }

    public function postDetails()
    {
        $this->set('page_title', 'Post Details');
        $this->set('description', '');
    }

    public function tableBootstrapBasic()
    {
        $this->set('page_title', 'Bootstrap');
        $this->set('description', '');
    }

    public function tableDatatableBasic()
    {
        $this->set('page_title', 'Datatable');
        $this->set('description', '');
    }

    public function ucLightgallery()
    {
        $this->set('page_title', 'Datatable');
        $this->set('description', '');
    }

    public function ucNestable()
    {
        $this->set('page_title', 'Nestable');
        $this->set('description', '');
    }

    public function ucNouiSlider()
    {
        $this->set('page_title', 'NouiSlider');
        $this->set('description', '');
    }

    public function ucSelect2()
    {
        $this->set('page_title', 'Select2');
        $this->set('description', '');
    }

    public function ucSweetalert()
    {
        $this->set('page_title', 'Sweet Alert');
        $this->set('description', '');
    }

    public function ucToastr()
    {
        $this->set('page_title', 'Toastr');
        $this->set('description', '');
    }

    public function uiAccordion()
    {
        $this->set('page_title', 'Accordion');
        $this->set('description', '');
    }

    public function uiAlert()
    {
        $this->set('page_title', 'Alert');
        $this->set('description', '');
    }

    public function uiBadge()
    {
        $this->set('page_title', 'Badge');
        $this->set('description', '');
        $this->set('bodyClass', 'badge-demo');
    }

    public function uiButton()
    {
        $this->set('page_title', 'Button');
        $this->set('description', '');
        $this->set('bodyClass', 'btn-page');
    }

    public function uiButtonGroup()
    {
        $this->set('page_title', 'Button Group');
        $this->set('description', '');
    }

    public function uiCard()
    {
        $this->set('page_title', 'Card');
        $this->set('description', '');
    }

    public function uiCarousel()
    {
        $this->set('page_title', 'Carousel');
        $this->set('description', '');
    }

    public function uiDropdown()
    {
        $this->set('page_title', 'Dropdown');
        $this->set('description', '');
    }

    public function uiGrid()
    {
        $this->set('page_title', 'Grid');
        $this->set('description', '');
    }

    public function uiListGroup()
    {
        $this->set('page_title', 'List Group');
        $this->set('description', '');
    }

    public function uiModal()
    {
        $this->set('page_title', 'Modal');
        $this->set('description', '');
    }

    public function uiPagination()
    {
        $this->set('page_title', 'Pagination');
        $this->set('description', '');
    }

    public function uiPopover()
    {
        $this->set('page_title', 'Popover');
        $this->set('description', '');
    }

    public function uiProgressbar()
    {
        $this->set('page_title', 'Progressbar');
        $this->set('description', '');
    }

    public function uiTab()
    {
        $this->set('page_title', 'Tab');
        $this->set('description', '');
    }

    public function uiTypography()
    {
        $this->set('page_title', 'Typography');
        $this->set('description', '');
    }

    public function widgetBasic()
    {
        $this->set('page_title', 'widget Basic');
        $this->set('description', '');
    }

    /**
     * View method
     *
     * @param string|null $id Sego Admin id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view(?string $id = null)
    {
        $segoAdmin = $this->SegoAdmin->get($id, contain: []);
        $this->set(compact('segoAdmin'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $segoAdmin = $this->SegoAdmin->newEmptyEntity();
        if ($this->request->is('post')) {
            $segoAdmin = $this->SegoAdmin->patchEntity($segoAdmin, $this->request->getData());
            if ($this->SegoAdmin->save($segoAdmin)) {
                $this->Flash->success(__('The sego admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sego admin could not be saved. Please, try again.'));
        }
        $this->set(compact('segoAdmin'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sego Admin id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit(?string $id = null)
    {
        $segoAdmin = $this->SegoAdmin->get($id, contain: []);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $segoAdmin = $this->SegoAdmin->patchEntity($segoAdmin, $this->request->getData());
            if ($this->SegoAdmin->save($segoAdmin)) {
                $this->Flash->success(__('The sego admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sego admin could not be saved. Please, try again.'));
        }
        $this->set(compact('segoAdmin'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sego Admin id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete(?string $id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $segoAdmin = $this->SegoAdmin->get($id);
        if ($this->SegoAdmin->delete($segoAdmin)) {
            $this->Flash->success(__('The sego admin has been deleted.'));
        } else {
            $this->Flash->error(__('The sego admin could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
